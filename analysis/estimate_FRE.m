clear; clc; close all;
%% files
rawPath = '/afm01/Q1/Q1709/MRA/data';
subjectID = 'voxelSize';
rawDir = 'bias_correction';
segDir = 'vessel_segmentation_intensity';

%% load data
ID = 23;
% raw
rawName = dir(fullfile(rawPath, subjectID, rawDir, ['TOF*', num2str(ID), '_biasCor.nii']));
m = MrImage(fullfile(rawName.folder, rawName.name));

%% load ref data
% raw
refName = dir(fullfile(rawPath, subjectID, rawDir, 'TOF*23_biasCor.nii'));
r = MrImage(fullfile(refName.folder, refName.name));
% load vessel mask
vesselMaskOrig = MrImage('/afm01/Q1/Q1709/MRA/results/ISMRM2020/maskV_23.nii');

%% plot data
plotThres = 500;
r.maxip('z').plot('rotate90', 1, 'displayRange', [0 plotThres]);
m.maxip(3).plot('rotate90', 1, 'displayRange', [0 plotThres]);

if ID ~=23
%% align data
% invT = m --> ref
% ref = m.*invT
% m = ref.*T
% estimate transformation matrix
[mC, trafo] = m.coregister_to(r);
mC.maxip(3).plot('rotate90', 1, 'displayRange', [0 plotThres]);

% apply to ref data and mask
vesselMask = vesselMaskOrig.copyobj();
vesselMask.affineTransformation.apply_transformation(trafo);
vesselMask = vesselMask.reslice(m, 'interpolation', 1).binarize(0.1);
m.maxip('z').plot('rotate90', 1, 'overlayImages', vesselMask.maxip('z'));

else
    vesselMask = vesselMaskOrig;
    m = r;
end
%% analyse data
surroundMask = vesselMask.imdilate(strel('disk', 1));
surroundMask = surroundMask - vesselMask;
iVessel = m.data(logical(vesselMask.data));
iBackground = m.data(logical(surroundMask.data));
relFRE = (mean(iVessel) - mean(iBackground))./ mean(iBackground);
disp(['relative FRE: ', num2str(relFRE)]);

%% plot data
if ID == 23
    sli = 41:65;
    sli_single = 58;
    displayRange = [0 300];
elseif ID == 27
    sli = 28:43;
    sli_single = 41;
    displayRange = [50 250];
elseif ID == 31
    sli = 25:37;
    sli_single = 36;
    displayRange = [0 220];
elseif ID == 35
    sli = 18:26;
    sli_single = 25;
    displayRange = [50 270];
end

close all;
m.select('z', sli).maxip(3).plot('rotate90', 1, 'displayRange', displayRange);
m.select('z', sli_single).plot('rotate90', 1, 'displayRange', displayRange);
m.select('z', sli).maxip(3).plot('rotate90', 1, 'displayRange', displayRange, ...
    'overlayImages', vesselMask.maxip('z'));
