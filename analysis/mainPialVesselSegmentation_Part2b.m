%% Final segmentation
clear; close all; clc;
%% 0) Input filename and parameters
dataPath = '/clusterdata/uqsboll2/data/MRABOSTON-Q1709/MRA/data_Magdeburg/Subject_02';
biasCorDir = 'biasCor_noiseCor';
%maskDir = 'masks';
fileID = 7;
filename = dir(fullfile(dataPath, biasCorDir, ['*_', num2str(fileID), '_biasCor_noiseCor.nii']));
vesselThreshold = 450;
lowVesselThreshold = 370;
vesselExcludeNPixel = 10;
nIter = 20;
s = {'rotate90', 1, 'colorBar', 'on'};
doSave = 0;
%% 1) Load Bias field corrected image 
[~,name,ext] = fileparts(filename.name);
biasCorImage = MrImage(fullfile(dataPath, biasCorDir, filename.name));

% hm: data already mask in repository
maskedImage = biasCorImage;
%maskFilename = dir(fullfile(dataPath, maskDir, ['*_', num2str(fileID), '_biasCor_mask.nii']));
%mask = MrImage(fullfile(dataPath, maskDir, maskFilename.name));
% apply mask to bias corrected image
%maskedImage = biasCorImage .* mask;

% check data
maskedImage.maxip('z').plot(s{:});
maskedImage.plot(s{:});


%% 2) Segment vessels via thresholding
% threshold and clean-up
vesselSegmentation_raw = maskedImage.binarize(vesselThreshold);
vesselSegmentation_raw = vesselSegmentation_raw.remove_clusters('nPixelRange', [0 vesselExcludeNPixel]);
vesselSegmentation_raw.maxip('z').plot(s{:});
%% 3) Improve segmentation using iterative region growing
% apply region growing
vesselSegmentation = biasCorImage.grow_region(...
    vesselSegmentation_raw, lowVesselThreshold, nIter);
vesselSegmentation.maxip('z').plot(s{:});
maskedImage.maxip('z').plot('overlayImages', vesselSegmentation.maxip('z'), ...
    'overlayMode', 'edge', 'rotate90', 1);

% save results
if doSave
    vesselSegmentation.parameters.save.path = fullfile(dataPath, 'vesselSegInt_denIm');
    vesselSegmentation.parameters.save.fileName = ['seg_', name, '_VT', ...
        num2str(vesselThreshold), '_lVT', num2str(lowVesselThreshold), '_VENP', ...
        num2str(vesselExcludeNPixel), ext];
    vesselSegmentation.save();
end
%% 4) Display final results
close all;
% segmentation
vesselSegmentation.name = ['segmentation of ', vesselSegmentation.name];
vesselSegmentation.maxip('z').plot('rotate90', 1, 'plotType', 'montage');
vesselSegmentation.maxip('x').flip('z').plot('sliceDimension', 'x', ...
    'plotType', 'montage');

% overlay
maskedImage.name = ['segmentation of ', maskedImage.name];
maskedImage.maxip('z').plot('overlayImages', vesselSegmentation.maxip('z'), 'overlayMode', 'edge', ...
    'rotate90', 1, 'plotType', 'montage');

% rendering
% create meshgrid
samplingPoints = vesselSegmentation.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fv = isosurface(X, Y, Z, permute(vesselSegmentation.data, [2, 1, 3]), 0.5);
% plot
figure;
p = patch(fv);
p.FaceColor = 'red';
p.EdgeColor = 'none';
camlight;
daspect([1 1 1]);
axis off;
view(55, 25);