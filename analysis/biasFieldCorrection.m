function outputFilename = biasFieldCorrection(inputFilename,varargin)
% BIASFIELDCORRECTION Applies ANTS N4BiasFieldCorrection on TOF data
% Requires N4BiasFieldCorrection installed and on the path
% A mask filename name can be given as well

% check if mask is available
hasMask = ~isempty(varargin);

% build output filename
[filepath,name,ext] = fileparts(inputFilename);
pathparts = strsplit(filepath,filesep);
outputPath = fullfile(filesep, pathparts{1:end-1}, 'bias_correction');
if hasMask
    outputPath = [outputPath, '_mask'];
    outputFilename = fullfile(outputPath, [name, '_biasCor_mask', ext]);
else
    outputFilename = fullfile(outputPath, [name, '_biasCor', ext]);
end



% check if output file exists
if isfile(outputFilename)
    disp('File already exists.');
else
    
    % check if output path exists
    if ~isfolder(outputPath)
        mkdir(outputPath)
    end
    
    % build command string for ants
    dim = '3';
    
    if hasMask
        cmd_string = ['N4BiasFieldCorrection -d ', dim, ' -i ', inputFilename, ...
            ' -o ', outputFilename, ' -x ', varargin{1}, ' -v 1'];
    else
        cmd_string = ['N4BiasFieldCorrection -d ', dim, ' -i ', inputFilename, ...
            ' -o ', outputFilename, ' -v 1'];
    end
    
    disp(['Running: ', cmd_string]);
    % execute
    system(cmd_string, '-echo');
    
end
end
