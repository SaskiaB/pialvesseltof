t = Tiff('vestimateVesselDensity-01.tif');
imageData = read(t);

magI = sum(imageData, 3);
f = figure; imagesc(magI);
% veins
v = imageData(:,:,1) > 80;
figure; imagesc(v);
vd = imdilate(v, strel('disk',5));

% remove veins
magIv = magI;
magIv(vd) = 0;
figure; imagesc(magIv);

% arteries
a = magIv > 40;
figure; imagesc(a);

% mask
p = drawpolygon(f.CurrentAxes);
m = p.createMask();

% mask arteries and clean-up
am = m.*a;
am = bwareaopen(am, 30);
figure; imagesc(am);

% mask veins and clean-up
vm = m.*v;
f2 = figure; imagesc(vm)
for n = 1:9
pn = drawpolygon(f2.CurrentAxes);
mn(:,:,n) = pn.createMask();
end
mn = logical(sum(mn, 3));
vm(mn) = 0;
figure; imagesc(vm);

% area of cortex covered with arteries
disp(['Area of cortex covered with arteries: ' ...
    num2str(sum(am(:))./sum(m(:)) * 100), ' %']);

% area of cortex covered with veins
disp(['Area of cortex covered with veins: ' ...
    num2str(sum(vm(:))./sum(m(:)) * 100), ' %']);
