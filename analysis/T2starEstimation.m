clear; close all; clc;
%% 0) Input filename and parameters
dataPath = '/QRISdata/Q1709/MRA/data_Magdeburg/Subject_02';
rawDir = 'GRE_TOF';
T2StarDir = 'T2star';
fileID = 15;
files = dir(fullfile(dataPath, rawDir, ['*_', num2str(fileID), '*.nii']));
doSave = 1;
%% 1) Load data and combine
for n = 1:numel(files)
    [~, name, ext] = fileparts(files(n).name);
    tmp{n} = MrImage(fullfile(dataPath, rawDir, files(n).name));
    val = jsondecode(fileread(fullfile(dataPath, rawDir, [name, '.json'])));
    tmp{n}.dimInfo.add_dims(tmp{n}.dimInfo.nDims + 1, ...
        'dimLabels', 'TE', 'samplingPoints', val.EchoTime);
end
% last slice in second echo might be missing due to recon error
% remove in first echo as well to allow combination
if tmp{1}.dimInfo.nSamples('z') ~= tmp{2}.dimInfo.nSamples('z')
    tmp{1} = tmp{1}.select('z', 1:tmp{1}.dimInfo.nSamples('z') - 1);
    tmp{1}.dimInfo.samplingPoints(3) = tmp{2}.dimInfo.samplingPoints(3);
    tmp{1}.affineTransformation.update_from_affine_matrix(tmp{2}.affineTransformation.affineMatrix);
end

% combine images
I = tmp{1}.combine(tmp);

%% 2) Check data
% check data
I.plot('TE', 1, 'y', 100, 'sliceDimension', 'y', 'rotate90', 1);
I.plot('TE', 2, 'y', 100, 'sliceDimension', 'y', 'rotate90', 1);
I.select('TE', 1).maxip('z').plot('rotate90', 1);
I.select('TE', 2).maxip('z').plot('rotate90', 1);
I.plot('TE', 1, 'z', 50, 'rotate90', 1);

%% 3) Compute T2*
% compute Delta TE
DeltaTE = diff(cell2mat(I.dimInfo.samplingPoints('TE')));
% compute T2 star map
T2Star = (log(I.select('TE', 1)./I.select('TE', 2))./DeltaTE).^(-1);
% remove TE dimension
T2Star = T2Star.remove_dims();
% plot results
plot(T2Star.*1000, 'y', 100, 'sliceDimension', 'y', 'rotate90', 1, 'displayRange', [0 20]);
plot(T2Star.*1000, 'z', 50, 'rotate90', 1, 'displayRange', [0 20]);

%% 4) Save results
if doSave
    T2Star.parameters.save.path = fullfile(dataPath, T2StarDir);
    T2Star.parameters.save.fileName = [name, '_T2Star', ext];
    T2Star.save();
end
