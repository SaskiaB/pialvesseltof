clear; clc; close all;
%% files
rawPath = '/afm01/Q1/Q1709/MRA/data';
subjectID = 'voxelSize';
rawDir = 'bias_correction';
segDir = 'vessel_segmentation_intensity';

%% load data 27 31 35
for ID = [23, 27, 31, 35]
    % raw
    rawName = dir(fullfile(rawPath, subjectID, rawDir, ['TOF*', num2str(ID), '_biasCor.nii']));
    raw = MrImage(fullfile(rawName.folder, rawName.name));
    % vessel segmentation
    segName = dir(fullfile(rawPath, subjectID, segDir, ['seg_TOF*', num2str(ID), '_biasCor_*.nii']));
    % combine
    if ID == 23
        m300 = raw;
        seg300 = MrImage(fullfile(segName.folder, segName.name));
    elseif ID == 27
        m400 = raw;
        fovMask400 = mask.copyobj();
        seg400 = MrImage(fullfile(segName.folder, segName.name));
    elseif ID == 31
        m500 = raw;
        seg500 = MrImage(fullfile(segName.folder, segName.name));
    elseif ID == 35
        m800 = raw;
        seg800 = MrImage(fullfile(segName.folder, segName.name));
    end
end


%% align ID 27 (400um) as this has the smalles FOV and create FOV mask
[mC300, trafo300] = m400.coregister_to(m300);
[mC500, trafo500] = m400.coregister_to(m500);
[mC800, trafo800] = m400.coregister_to(m800);

m400.maxip('z').plot('rotate90', 1);
mC300.maxip(3).plot('rotate90', 1);
mC500.maxip(3).plot('rotate90', 1);
mC800.maxip(3).plot('rotate90', 1);

% create FOV mask
fovMask300 = m400.copyobj();
fovMask300.affineTransformation.apply_transformation(trafo300);
fovMask300 = fovMask300.reslice(m300, 'interpolation', 1).binarize(0.5);
m300.plot('rotate90', 1);
fovMask300.plot('rotate90', 1);

fovMask500 = m400.copyobj();
fovMask500.affineTransformation.apply_transformation(trafo500);
fovMask500 = fovMask500.reslice(m500, 'interpolation', 1).binarize(0.5);
m500.plot('rotate90', 1);
fovMask500.plot('rotate90', 1);

fovMask800 = m400.copyobj();
fovMask800.affineTransformation.apply_transformation(trafo800);
fovMask800 = fovMask800.reslice(m800, 'interpolation', 1).binarize(0.5);
m800.plot('rotate90', 1);
fovMask800.plot('rotate90', 1);

% apply mask
m300FOV = m300.*fovMask300;
seg300FOV = seg300.*fovMask300;

m500FOV = m500.*fovMask500;
seg500FOV = seg500.*fovMask500;

m800FOV = m800.*fovMask800;
seg800FOV = seg800.*fovMask800;

%% skeletonize
m400.maxip('z').plot('rotate90', 1, 'overlayImages', seg400.maxip('z'));
m300FOV.maxip('z').plot('rotate90', 1, 'overlayImages', seg300FOV.maxip('z'));
m500FOV.maxip('z').plot('rotate90', 1, 'overlayImages', seg500FOV.maxip('z'));
m800FOV.maxip('z').plot('rotate90', 1, 'overlayImages', seg800FOV.maxip('z'));

skel400 = seg400.copyobj();
skel400.data = logical(skel400.data);
skel400 = skel400.perform_unary_operation(@bwskel, '3d');

skel300 = seg300FOV.copyobj();
skel300.data = logical(skel300.data);
skel300 = skel300.perform_unary_operation(@bwskel, '3d');

skel500 = seg500FOV.copyobj();
skel500.data = logical(skel500.data);
skel500 = skel500.perform_unary_operation(@bwskel, '3d');

skel800 = seg800FOV.copyobj();
skel800.data = logical(skel800.data);
skel800 = skel800.perform_unary_operation(@bwskel, '3d');

m400.maxip('z').plot('rotate90', 1, 'overlayImages', skel400.maxip('z'), 'plotType', 'montage');
m300FOV.maxip('z').plot('rotate90', 1, 'overlayImages', skel300.maxip('z'), 'plotType', 'montage');
m500FOV.maxip('z').plot('rotate90', 1, 'overlayImages', skel500.maxip('z'), 'plotType', 'montage');
m800FOV.maxip('z').plot('rotate90', 1, 'overlayImages', skel800.maxip('z'), 'plotType', 'montage');

disp(['400 rel to 300: ', ...
    num2str((sum(skel400.data(:)).*0.4)/(sum(skel300.data(:)).*0.3)*100, '%.0f'), '%']);
disp(['500 rel to 300: ', ...
    num2str((sum(skel500.data(:)).*0.5)/(sum(skel300.data(:)).*0.3)*100, '%.0f'), '%']);
disp(['800 rel to 300: ', ...
    num2str((sum(skel800.data(:)).*0.8)/(sum(skel300.data(:)).*0.3)*100, '%.0f'), '%']);

disp(['300 rel to 400: ', ...
    num2str(((sum(skel300.data(:)).*0.3)/(sum(skel400.data(:)).*0.4)-1)*100, '%.0f'), '%']);
disp(['300 rel to 500: ', ...
    num2str(((sum(skel300.data(:)).*0.3)/(sum(skel500.data(:)).*0.5)-1)*100, '%.0f'), '%']);
disp(['300 rel to 800: ', ...
    num2str(((sum(skel300.data(:)).*0.3)/(sum(skel800.data(:)).*0.8)-1)*100, '%.0f'), '%']);

disp(['300 rel to 400: ', ...
    num2str(((sum(skel300.data(:)).*0.3)/(sum(skel400.data(:)).*0.4)-1)*100, '%.0f'), '%']);
disp(['400 rel to 500: ', ...
    num2str(((sum(skel400.data(:)).*0.4)/(sum(skel500.data(:)).*0.5)-1)*100, '%.0f'), '%']);
disp(['500 rel to 800: ', ...
    num2str(((sum(skel500.data(:)).*0.5)/(sum(skel800.data(:)).*0.8)-1)*100, '%.0f'), '%']);
%% 160um vs 140um
clear; clc; % close all;
%% files
rawPath = '/afm01/Q1/Q1709/MRA/data_Magdeburg/';
subjectID = 'Subject_02';
rawDir = 'biasCorrection';
segDir = 'vesselSegmentation';

% 140um
for ID = [7, 19]
    % raw
    rawName = dir(fullfile(rawPath, subjectID, rawDir, ['TOF*', num2str(ID), '_biasCor.nii']));
    raw = MrImage(fullfile(rawName.folder, rawName.name));
    % vessel segmentation
    segName = dir(fullfile(rawPath, subjectID, segDir, ['TOF*', num2str(ID), '_biasCor_*.nii']));
    % combine
    if ID == 7
        m140 = raw;
        seg140 = MrImage(fullfile(segName(1).folder, segName(1).name));
    elseif ID == 19
        m160 = raw;
        fovMask160 = mask.copyobj();
        seg160 = MrImage(fullfile(segName.folder, segName.name));
    end
end

%% align ID 19 (160um) as this has the smalles FOV and create FOV mask
[mC140, trafo140] = m160.coregister_to(m140, 'trafoParameters', 'translation');

m160.maxip('z').plot('rotate90', 1);
mC140.maxip(3).plot('rotate90', 1);

% create FOV mask
fovMask140 = m160.copyobj();
fovMask140.affineTransformation.apply_inverse_transformation(trafo140);
fovMask140 = fovMask140.reslice(m140, 'interpolation', 1).binarize(0.5);
m140.plot('rotate90', 1);
fovMask140.plot('rotate90', 1);

% apply mask
m140FOV = m140.*fovMask140;
seg140FOV = seg140.*fovMask140;

%% skeletonize
m160.maxip('z').plot('rotate90', 1, 'overlayImages', seg160.maxip('z'));
m140FOV.maxip('z').plot('rotate90', 1, 'overlayImages', seg140FOV.maxip('z'));

skel160 = seg160.copyobj();
skel160.data = logical(skel160.data);
skel160 = skel160.perform_unary_operation(@bwskel, '3d');

skel140 = seg140FOV.copyobj();
skel140.data = logical(skel140.data);
skel140 = skel140.perform_unary_operation(@bwskel, '3d');

m160.maxip('z').plot('rotate90', 1, 'overlayImages', skel160.maxip('z'), 'plotType', 'montage');
m140FOV.maxip('z').plot('rotate90', 1, 'overlayImages', skel140.maxip('z'), 'plotType', 'montage');

disp(['140 rel to 160: ', ...
    num2str(((sum(skel140.data(:)).*0.14)/(sum(skel160.data(:)).*0.16)-1)*100, '%.0f'), '%']);
