%% Register a low-resolution TOF to a high-resolution TOF
% to use the second echo of the low-resolution TOF for vein removal
clear; close all; clc;

% hm: data in repository already masked; therefore, code adapted

%% 0) Input filename and parameters
dataPath = '/QRISdata/Q1709/MRA/data_Magdeburg/Subject_02';
biasCorDir = 'biasCorrection';
%maskDir = 'masks';
stationaryID = 7;   % high-res TOF
movingID = 19;      % low-res TOF

applyTo = {'T2Star', 'orig'};
% applyTo = {'T2Star', 'mask', 'orig'};
doSave = 0;

stationaryName = dir(fullfile(dataPath, biasCorDir, ...
    ['*', num2str(stationaryID), '*_biasCor.nii']));
stationaryName = stationaryName.name;
movingName = dir(fullfile(dataPath, biasCorDir, ...
    ['*', num2str(movingID), '*_biasCor.nii']));
movingName = movingName.name;
% hm: data in repository already masked
%stationaryMask = dir(fullfile(dataPath, maskDir, ...
%    ['*', num2str(stationaryID), '*_biasCor_mask.nii']));
%stationaryMask = stationaryMask.name;
%movingMask = dir(fullfile(dataPath, maskDir, ...
%    ['*', num2str(movingID), '*_biasCor_mask.nii']));
%movingMask = movingMask.name;

%% 1) Load images
Istat = MrImage(fullfile(dataPath, biasCorDir, stationaryName));
Istat.parameters.save.path = '/gpfs1/scratch/30days/uqsboll2/computeHere';
Istat.name = 'high-res reference';
Istat.maxip('z').plot('rotate90',1);

Imove = MrImage(fullfile(dataPath, biasCorDir, movingName));
Imove.parameters.save.path = '/gpfs1/scratch/30days/uqsboll2/computeHere';
Imove.name = 'low-res move';
Imove.maxip('z').plot('rotate90', 1);

%% 2) Downsample high-res image
IstatDS = Istat.reslice(Imove);
IstatDS.name = 'high-res reference downsampled';
IstatDS.maxip('z').plot('rotate90', 1);
%% 3) Coregister low-res to downsample high-res image
[ImoveC, affineTrafo] = Imove.coregister_to(IstatDS, 'trafoParameters', 'affine', ...
    'separation', [1 0.5 0.2], 'objectiveFunction', 'ncc');
ImoveC.name = 'low-res corgistered and reslice';
ImoveC.maxip('z').plot('rotate90', 1);
%% 4) Apply affine transformation matrix
% T2 Star image
if any(ismember(applyTo, 'T2Star'))
    T2StarDir = 'T2star';
    T2StarName = dir(fullfile(dataPath, T2StarDir, ...
        ['*', num2str(movingID), '*_T2Star.nii']));
    T2Star = MrImage(fullfile(dataPath, T2StarDir, T2StarName.name));
    T2Star.name = 'low-res T2 Star image';
    T2Star.plot('z', 34, 'rotate90', 1, 'displayRange', [0 0.02]);
    
    % transform dummy image to test transformation
    testTransformation = Imove.copyobj();
    testTransformation.name = 'test transformation - this should match low-res coregistered and reslices';
    testTransformation.affineTransformation.apply_inverse_transformation(affineTrafo);
    testTransformationResliced = testTransformation.reslice(Imove);
    testTransformationResliced.maxip('z').plot('rotate90', 1);
    
    % apply transformation to T2star image
    T2Star.affineTransformation.apply_inverse_transformation(affineTrafo);
    T2StarC = T2Star.reslice(Imove, 'interpolation', 0);
    T2StarC.plot('z', 34, 'rotate90', 1, 'displayRange', [0 0.02]);
    
    % reslice T2star image to high res original
    T2StarHR = T2Star.reslice(Istat, 'interpolation', 0);
    T2StarHR.plot('z', 100, 'rotate90', 1, 'displayRange', [0 0.02]);
    
    if doSave
        T2StarHR.parameters.save.path = fullfile(dataPath, T2StarDir);
        [~, name, ext] = fileparts(T2StarName.name);
        T2StarHR.parameters.save.fileName = [name, 'alignedTo', num2str(stationaryID), ext];
        T2StarHR.save();
    end
end

if any(ismember(applyTo, 'orig'))
    ImoveC2 = Imove.copyobj();
    ImoveC2.affineTransformation = ImoveC2.affineTransformation.apply_inverse_transformation(affineTrafo);
    ImoveHR = ImoveC2.reslice(Istat, 'interpolation', 0);
    ImoveHR.name = 'low-res coregistered and upsampled';
    ImoveHR.maxip('z').plot('rotate90', 1);
    Istat.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x');
    ImoveHR.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x');
    if doSave
        ImoveHR.parameters.save.path = fullfile(dataPath, biasCorDir);
        [~, name, ext] = fileparts(movingName);
        ImoveHR.parameters.save.fileName = [name, 'alignedTo', num2str(stationaryID), ext];
        ImoveHR.save();
    end    
end

if any(ismember(applyTo, 'mask'))
    MmoveC = Mmove.copyobj();
    MmoveC.affineTransformation = MmoveC.affineTransformation.apply_inverse_transformation(affineTrafo);
    MmoveHR = MmoveC.reslice(Istat, 'interpolation', 0);
    MmoveHR.name = 'low-res coregistered and upsampled';
    MmoveHR.maxip('z').plot('rotate90', 1);
    Istat.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x');
    MmoveHR.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x');
    if doSave
        MmoveHR.parameters.save.path = fullfile(dataPath, maskDir);
        [~, name, ext] = fileparts(movingMask);
        MmoveHR.parameters.save.fileName = [name, 'alignedTo', num2str(stationaryID), ext];
        MmoveHR.save();
    end    
end
