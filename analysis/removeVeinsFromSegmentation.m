clear; clc; close all;
%% 0) Input filename and parameters
% hm: data in repository already masked; therefore, code adapted
dataPath = '/clusterdata/uqsboll2/data/MRABOSTON-Q1709/MRA/data_Magdeburg/Subject_02';
T2StarDir = 'T2star';
segDir = 'vesselSegInt_denIm';
fileID = 7;
T2StarFiles = dir(fullfile(dataPath, T2StarDir, ['*_', num2str(fileID), '*T2Star.nii']));
if isempty(T2StarFiles)
    T2StarFiles = dir(fullfile(dataPath, T2StarDir, ...
        ['*T2StaralignedTo', num2str(fileID), '.nii']));
end
segFiles = dir(fullfile(dataPath, segDir, ['*_', num2str(fileID), '*_biasCor*.nii']));
biasCorDir = 'biasCorrection';
biasCorFiles = dir(fullfile(dataPath, biasCorDir, ['*_', num2str(fileID), '*.nii']));
%maskDir = 'masks';
%maskFiles = dir(fullfile(dataPath, maskDir, ['*_', num2str(fileID), '*_mask.nii']));
doSave = 0;
verbose = 1;
T2StarThreshold_ms = 27;
%% 1) Load data
T2Star = MrImage(fullfile(dataPath, T2StarDir, T2StarFiles.name));
seg = MrImage(fullfile(dataPath, segDir, segFiles(1).name));
biasCor = MrImage(fullfile(dataPath, biasCorDir, biasCorFiles(1).name));
% last slice in T2Star might be missing due to recon error
% remove in seg as well to allow combination
if T2Star.dimInfo.nSamples('z') ~= seg.dimInfo.nSamples('z')
    seg = seg.select('z', 1:seg.dimInfo.nSamples('z') - 1);
    seg.dimInfo.samplingPoints(3) = T2Star.dimInfo.samplingPoints(3);
    seg.affineTransformation.update_from_affine_matrix(T2Star.affineTransformation.affineMatrix);
end
% remove in biasCor as well to allow combination
if T2Star.dimInfo.nSamples('z') ~= biasCor.dimInfo.nSamples('z')
    biasCor = biasCor.select('z', 1:biasCor.dimInfo.nSamples('z') - 1);
    biasCor.dimInfo.samplingPoints(3) = T2Star.dimInfo.samplingPoints(3);
    biasCor.affineTransformation.update_from_affine_matrix(T2Star.affineTransformation.affineMatrix);
end
% hm: data in repository already masked
% remove in mask as well to allow combination
% if T2Star.dimInfo.nSamples('z') ~= mask.dimInfo.nSamples('z')
%     mask = mask.select('z', 1:mask.dimInfo.nSamples('z') - 1);
%     mask.dimInfo.samplingPoints(3) = T2Star.dimInfo.samplingPoints(3);
%     mask.affineTransformation.update_from_affine_matrix(T2Star.affineTransformation.affineMatrix);
% end


%% 2) Combine data
% created segmentation with T2Star values for visualisation
V = T2Star.copyobj();
V.data(isinf(V.data)) = 0;
V.data(isnan(V.data)) = 0;
V = V.*seg.select('z', 1:seg.dimInfo.nSamples('z')- 1).*1000;
V.plot('y', 100, 'sliceDimension', 'y', 'rotate90', 1, 'displayRange', [0 20]);
V.maxip('z').plot('rotate90', 1);

%% 3) Split segmentation into arteries and veins
% prepare empty vein objects
seg.maxip('z').plot('rotate90', 1);
veins = seg.copyobj();
veins.data = zeros(veins.dimInfo.nSamples);
tmp = veins.copyobj();
% extract region properties
stats = seg.regionprops3('conn', 6, 'props', {'Volume', 'VoxelIdxList', 'Solidity'});
[Volume, ID] = sort(stats.Volume, 'descend');

% check each region and include as vein if the 90%-percentile is below the
% T2Star threshold
for n = 1:numel(Volume)
    T2StarData = T2Star.data(stats.VoxelIdxList{ID(n)});
    if verbose && n < 20
        tmp.data(stats.VoxelIdxList{ID(n)}) = 1;
        tmp.name = ['90 Percentile: ' num2str(prctile(T2StarData*1000, 90)), ...
            'Volume', num2str(Volume(n))];
        tmp.maxip('z').plot('rotate90', 1);
        tmp.data = zeros(tmp.dimInfo.nSamples);
    end
    if prctile(T2StarData, 90) < T2StarThreshold_ms/1000 && stats.Volume(ID(n))
        veins.data(stats.VoxelIdxList{ID(n)}) = 1;
    end
    T2Starprctile(n) = prctile(T2StarData*1000, 90);
end

% remove veins from segmentation
arteries = seg - veins;

% apply mask to bias corrected image
biasCorM = biasCor;
%biasCorM = biasCor .* mask; % hm: data in repository already masked

%% 4) Plot results
% plot raw results
arteries.maxip('z').plot('rotate90', 1, 'overlayImages', veins.maxip('z'));
% make artery outline
arteriesOutline = arteries.imdilate(strel('disk', 1));
biasCorM.maxip('z').plot('overlayImages', veins.maxip('z'), 'rotate90', 1, ...
    'overlayMode', 'edge', 'plotType', 'montage');
biasCorM.maxip('z').plot('overlayImages', arteries.maxip('z'), 'rotate90', 1, ...
    'overlayMode', 'edge', 'plotType', 'montage');
biasCorM.maxip('y').plot('overlayImages', veins.maxip('y'), 'rotate90', 1, ...
    'overlayMode', 'edge', 'sliceDimension', 'y', 'plotType', 'montage');
biasCorM.maxip('y').plot('overlayImages', ...
    arteriesOutline.maxip('y') - arteries.maxip('y'), 'rotate90', 1, ...
    'overlayMode', 'mask', 'sliceDimension', 'y', 'plotType', 'montage');
biasCorM.maxip('x').plot('overlayImages', veins.maxip('x'), 'rotate90', 2, ...
    'overlayMode', 'edge', 'sliceDimension', 'x', 'plotType', 'montage');
biasCorM.maxip('x').plot('overlayImages', ...
    arteriesOutline.maxip('x') -  arteries.maxip('x'), 'rotate90', 2, ...
    'overlayMode', 'mask', 'sliceDimension', 'x', 'plotType', 'montage');
% remove veins from data
biasCorMCP = biasCorM.copyobj();
biasCorMCP.data(logical(veins.data)) = 0;
biasCorMCP.maxip('z').plot('rotate90', 1, 'plotType', 'montage');
biasCorMCP.maxip('y').plot('rotate90', 1, 'sliceDimension', 'y', 'plotType', 'montage');
biasCorMCP.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x', 'plotType', 'montage');
%% 5) Save results
if doSave
    arteries.parameters.save.path = fullfile(dataPath, segDir, 'arteries');
    arteries.parameters.save.fileName = ['arteries_', segFiles(1).name];
    arteries.save();
    veins.parameters.save.path = fullfile(dataPath, segDir, 'veins');
    veins.parameters.save.fileName = ['veins_', segFiles(1).name];
    veins.save();
    biasCorMCP.parameters.save.path = fullfile(dataPath, biasCorDir, 'arteries');
    biasCorMCP.parameters.save.fileName = ['arteries_', biasCorFiles(1).name];
    biasCorMCP.save();
end