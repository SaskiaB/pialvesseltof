%% Optionial second round of bias field correction using the mask generated
% in part 1

%% 0) Input filename and parameters
dataPath = '/QRISdata/Q1709/MRA/data/VASO_P07';
filename = 'GRE_TOF_3D_300um_TR21_FA18_TE7p85_15p7_sli60_FCY_GMP_BW200_43_e2.nii';
s = {'rotate90', 1, 'colorBar', 'on'};
%% 1) Bias field correction using ANTs N4BiasFieldCorrection
[~,name,ext] = fileparts(filename);
filenameBiasCor = biasFieldCorrection(fullfile(dataPath, 'GRE_TOF', filename));
%maskName = fullfile(dataPath, 'masks', [name, '_biasCor_mask', ext]);
%filenameBiasCor = biasFieldCorrection(fullfile(dataPath, 'GRE_TOF', filename), ...
%    maskName);

%% 2) Display Output
biasCorImage = MrImage(filenameBiasCor);
% first - check data
biasCorImage.maxip('z').plot(s{:});
biasCorImage.plot(s{:});