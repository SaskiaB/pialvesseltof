%% Mask generation using a threshold, minimum number of pixel included and
% percent area filled to generate brain mask
% final touches need to be manually performed afterwards

%% 0) Input filename and parameters
dataPath = '/afm01/Q1/Q1709/MRA/data/Subject_03';
rawDir = 'TOF';
fileID = 35;
filename = dir(fullfile(dataPath, rawDir, ['*_', num2str(fileID), '.nii']));
maskThreshold = 230;
maskExcludeNPixel = 30e3;
maskExcludePercentAreaFilled = 40;
s = {'rotate90', 1, 'colorBar', 'on'};
%% 1) Bias field correction using ANTs N4BiasFieldCorrection
filenameBiasCor = biasFieldCorrection(fullfile(dataPath, rawDir, filename.name));

%% 2) Mask generation
biasCorImage = MrImage(filenameBiasCor);
% first - check data
biasCorImage.maxip('z').plot(s{:});
biasCorImage.plot(s{:});

% binarize image
binarizedImage = biasCorImage.binarize(maskThreshold);
binarizedImage.plot(s{:});

% remove small clusters
cleanedMask = binarizedImage.remove_clusters('nPixelRange', [0 maskExcludeNPixel], ...
    'pAreaRange', [0 maskExcludePercentAreaFilled], 'applicationDimension', '2d');
cleanedMask.plot(s{:});
biasCorImage.plot('z', 23, 'overlayImages', cleanedMask, s{:});

% save mask
% we need to do a little bit of manual cleanup (e.g. using itksnap) to get
% rid of the skin surrounding the brain
% keep in mind that you only need remove those parts that touch the brain,
% everything else will automatically removed later
[~,name,ext] = fileparts(filename.name);
cleanedMask.parameters.save.path = fullfile(dataPath, 'masks');
cleanedMask.parameters.save.fileName = [name, '_biasCor_mask_raw', ext];
if isfile(cleanedMask.get_filename)
    disp('Cleaned Mask exists.');
else
    cleanedMask.save();
end

%% 3) Manual cleanup using itksnap or similar

%% 4) Final automatic cleanup
% load manually trimmed mask
manualMask = MrImage(fullfile(dataPath, 'masks', ...
    [name, '_biasCor_mask_manual', ext]));
manualMask.plot(s{:});
% now clean-up
mask = manualMask.remove_clusters('nPixelRange', [0 maskExcludeNPixel], ...
    'pAreaRange', [0 maskExcludePercentAreaFilled], 'applicationDimension', '2d');
mask.plot(s{:});
% dilate and fill
mask = mask.imdilate(strel('disk', 1)).imfill('holes');
mask.plot(s{:});

% check results
biasCorImage.plot('overlayImages', mask, 'overlayMode', 'edge', s{:});

% save final mask
mask.parameters.save.path = fullfile(dataPath, 'masks');
mask.parameters.save.fileName = [name, '_biasCor_mask', ext];
mask.save();