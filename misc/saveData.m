clear; clc; close all;
%% files
% hm: data in repository already masked; therefore, code adapted
rawPath = '/afm01/Q1/Q1709/MRA/data';
subjectID = 'voxelSize';
rawDir = 'TOF_3D';
biasCorDir = 'bias_correction';
%maskDir = 'masks';
segDir = 'vessel_segmentation_intensity';

%% load data
ID = 35;
% raw
rawName = dir(fullfile(rawPath, subjectID, rawDir, ['TOF*', num2str(ID), '.nii']));
raw = MrImage(fullfile(rawName.folder, rawName.name));
% biasCor
biasCorName = dir(fullfile(rawPath, subjectID, biasCorDir, ['TOF*', num2str(ID), '_biasCor.nii']));
biasCor = MrImage(fullfile(biasCorName.folder, biasCorName.name));
% mask
%maskName = dir(fullfile(rawPath, subjectID, maskDir, ['TOF*', num2str(ID), '_biasCor_mask.nii']));
%mask = MrImage(fullfile(maskName.folder, maskName.name));
% seg
segName = dir(fullfile(rawPath, subjectID, segDir, ['seg_TOF*', num2str(ID), '_biasCor_*.nii']));
seg = MrImage(fullfile(segName.folder, segName.name));

% plot
raw.maxip('z').plot('rotate90', 1);
biasCor.maxip('z').plot('rotate90', 1);
seg.maxip('z').plot('rotate90', 1);

% save
saveDir = '/scratch/cvl/uqsboll2/addData/voxelSize';
raw.parameters.save.path = fullfile(saveDir, 'raw');
biasCor.parameters.save.path = fullfile(saveDir, 'biasCor');
seg.parameters.save.path = fullfile(saveDir, 'seg');

raw.parameters.save.fileName = rawName.name;
biasCor.parameters.save.fileName = biasCorName.name;
seg.parameters.save.fileName = segName.name;

raw.get_filename()
biasCor.get_filename()
seg.get_filename()

raw.save();
biasCor.save();
seg.save();

%%
clear;
close all;
clc;

dataPath = '/afm01/Q1/Q1709/MRA/data/twoEchoTOF';
rawDir = 'TOF';
biasCorDir = 'bias_correction';
segDir = 'vessel_segmentation_intensity';
%maskDir = 'masks';
artDir = 'arteries';
veinDir = 'veins';
T2StarDir = 'T2star';

rawFilesEcho1 = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32.nii';
rawFilesEcho2 = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_e2.nii';
biasCorFilesEcho1 = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor.nii';
biasCorFilesEcho2 = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_e2_biasCor.nii';
%maskFiles = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_mask.nii';
T2StarFiles = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_e2_T2Star.nii';

segFiles = 'seg_GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_H75_L55_C10.nii';
artFiles = 'arteries_seg_GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_H75_L55_C10.nii';
veinFiles = 'veins_seg_GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_H75_L55_C10.nii';
%% 1) Load files
raw_e1 = MrImage(fullfile(dataPath, rawDir, rawFilesEcho1));
raw_e2 = MrImage(fullfile(dataPath, rawDir, rawFilesEcho2));
biasCor_e1 = MrImage(fullfile(dataPath, biasCorDir, biasCorFilesEcho1));
biasCor_e2 = MrImage(fullfile(dataPath, biasCorDir, biasCorFilesEcho2));
%mask = MrImage(fullfile(dataPath, maskDir, maskFiles));
T2star = MrImage(fullfile(dataPath, T2StarDir, T2StarFiles));

seg = MrImage(fullfile(dataPath, segDir, segFiles));
arteries = MrImage(fullfile(dataPath, segDir, artDir, artFiles));
veins = MrImage(fullfile(dataPath, segDir, veinDir, veinFiles));


% plot
raw_e1.maxip('z').plot('rotate90', 1);
raw_e2.maxip('z').plot('rotate90', 1);
biasCor_e1.maxip('z').plot('rotate90', 1);
biasCor_e2.maxip('z').plot('rotate90', 1);
T2star.maxip('z').plot('rotate90', 1);
seg.maxip('z').plot('rotate90', 1);
arteries.maxip('z').plot('rotate90', 1);
veins.maxip('z').plot('rotate90', 1);

% save
saveDir = '/scratch/cvl/uqsboll2/addData/twoEchoTOF';
raw_e1.parameters.save.path = fullfile(saveDir, 'raw');
raw_e2.parameters.save.path = fullfile(saveDir, 'raw');
biasCor_e1.parameters.save.path = fullfile(saveDir, 'biasCor');
biasCor_e2.parameters.save.path = fullfile(saveDir, 'biasCor');
T2star.parameters.save.path = fullfile(saveDir, 'T2star');
seg.parameters.save.path = fullfile(saveDir, 'seg');
arteries.parameters.save.path = fullfile(saveDir, 'seg', 'arteries');
veins.parameters.save.path = fullfile(saveDir, 'seg', 'veins');

raw_e1.parameters.save.fileName = rawFilesEcho1;
raw_e2.parameters.save.fileName = rawFilesEcho2;
biasCor_e1.parameters.save.fileName = biasCorFilesEcho1;
biasCor_e2.parameters.save.fileName = biasCorFilesEcho2;
T2star.parameters.save.fileName = T2StarFiles;
seg.parameters.save.fileName = segFiles;
arteries.parameters.save.fileName = artFiles;
veins.parameters.save.fileName = veinFiles;

raw_e1.get_filename()
raw_e2.get_filename()
biasCor_e1.get_filename()
biasCor_e2.get_filename()
T2star.get_filename()
seg.get_filename()
arteries.get_filename()
veins.get_filename()

raw_e1.save()
raw_e2.save()
biasCor_e1.save()
biasCor_e2.save()
T2star.save()
seg.save()
arteries.save()
veins.save()