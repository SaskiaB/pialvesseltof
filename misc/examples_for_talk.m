%% clean-up at start
clear; close all; clc;
%% parameters
M0 = 1; % thermal equilibrium magnetization before any RF pulse
TR_ms = 20; % repetition time in ms
T1_ms_gm = 1950; % longitudinal relaxation time of grey matter from (ref)
T1_ms_wm = 1100; % longitudinal relaxation time of blood from (ref)
T1_ms_blood = 2100; % longitudinal relaxation time of blood from (ref)
alphaErnst_gm = rad2deg(acos(exp(-TR_ms/T1_ms_gm))); % Ernst angle for grey matter
theta_deg = [10]; % selecton of excitation angles
mRF = 1:500;

%% simulating the approach to equilibrium
% longitudinal magnetization of gm
MzGM = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_gm, theta_deg, mRF');
% longitudinal magnetization of wm
MzWM = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_wm, theta_deg, mRF');

figure; plot(mRF, [MzGM, MzWM], 'LineWidth', 2);
ylim([0 1]); legend('Grey Matter', 'White Matter');
xlabel('Number of RF Pulses'); ylabel('Longitudinal Magnetization (M_z)');

%% simulating the effect of excitation flip angle
theta_deg = [10, 20, 30]; % selecton of excitation angles

% longitudinal magnetization of gm
MzGM = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_gm, theta_deg, mRF');

figure; plot(mRF, MzGM, 'LineWidth', 2);
ylim([0 1]); legend(['\theta = 10', char(176)], ['\theta = 20', char(176)], ...
    ['\theta = 30', char(176)]);
xlabel('Number of RF Pulses'); ylabel('Longitudinal Magnetization (M_z)');

figure; plot(mRF(1:50), MzGM(1:50, :), 'LineWidth', 2);
ylim([0 1]); legend(['\theta = 10', char(176)], ['\theta = 20', char(176)], ...
    ['\theta = 30', char(176)]);
xlabel('Number of RF Pulses'); ylabel('Longitudinal Magnetization (M_z)');

%% explaning FRE
theta_deg = 18; % selecton of excitation angles

% longitudinal magnetization of gm
MzGM = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_gm, theta_deg, mRF');

% longitudinal magnetization of moving blood water spins
MzB = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_blood, theta_deg, mRF');

figure; plot(mRF, [MzGM, MzB], 'LineWidth', 2);
ylim([0 1]); legend('grey matter', '(arterial) blood');
xlabel('Number of RF Pulses'); ylabel('Longitudinal Magnetization (M_z)');

