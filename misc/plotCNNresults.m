clear; close all; clc;

dataPath = '/afm02/Q3/Q3461/data/MRA_P11/slabs_added_together/eval';
resultsPath = '/afm02/Q3/Q3461/data/MRA_P11/slabs_added_together/3dcnn_seg_results/ckpt/step22500/eval';
biasCor = MrImage(fullfile(dataPath, '/created_bigimage_ALL.nii'));
mask = MrImage(fullfile(dataPath, 'all_slabs_Final_Mask_BigSpace_20210813.nii'));
seg = MrImage(fullfile(dataPath, 'seg_created_bigimage_ALL_denoised_VT300_lVT250_VENP10.nii'));
seg_cnn = MrImage(fullfile(resultsPath, 'test_3d_pred.nii'));
m = biasCor.*mask;
seg_cnn_clean = seg_cnn.remove_clusters('nPixelRange', [1 10]);
m.maxip('z').plot('rotate90', 1, 'plotType', 'montage', 'displayRange', [100 600]);

% plot segmentation only in 3D
samplingPoints = seg_cnn_clean.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(seg_cnn_clean.data, [2, 1, 3]), 0.5);

% plot
fig = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;
view(45, 40);
fig.CurrentAxes.CameraViewAngle = 10;

view(-45, 40);
