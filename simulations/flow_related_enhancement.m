%% clean-up at start
clear; close all; clc;
%% parameters
M0 = 1; % thermal equilibrium magnetization before any RF pulse
TR_ms = 20; % repetition time in ms
T1_ms_gm = 1950; % longitudinal relaxation time of grey matter from (ref)
T1_ms_blood = 2100; % longitudinal relaxation time of blood from (ref)
alphaErnst_gm = rad2deg(acos(exp(-TR_ms/T1_ms_gm))); % Ernst angle for grey matter
theta_deg = [alphaErnst_gm, 18, 30]; % selecton of excitation angles
mRF = 1:50;

%% simulating the flow related enhancement for different excitation flip angles
% equilibrium magnetization of the background tissue (grey matter)
MzEgm = compute_equilibrium_magnetization(M0, TR_ms, T1_ms_gm, theta_deg);

% longitudinal magnetization of moving blood water spins
MzB = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_blood, theta_deg, mRF');

% hm: downward compatibility for pre-MATLAB2016b (no implicit expansion)
MzEgm = repmat(MzEgm, size(mRF,2), 1);

% compute FRE
FRE = (MzB - MzEgm)./MzEgm;

% plot
fig = figure;
plot(mRF*TR_ms, FRE, 'LineWidth', 2);
% hm: downward compatibility for pre-MATLAB2018b (no yline function)
line([mRF(1)*TR_ms, mRF(end)*TR_ms], [0,0], ...
    'LineStyle','--', 'LineWidth', 2, 'Color', 'black');
ylabel('relative flow-related enhancement'); xlabel('blood delivery time (ms)');
title('Flow-related enhancement as a function of excitation flip angle \theta');
legend({['\theta = ', num2str(alphaErnst_gm, '%2.0f'), char(176), ' (Ernst angle)'], ...
    ['\theta = ', num2str(theta_deg(2), '%2.0f'), char(176)], ...
    ['\theta = ', num2str(theta_deg(3), '%2.0f'), char(176)]});
ylim([-0.5 4]);
position = fig.Position;
newPosition = [position(1:2), 860, position(4)];
fig.Position = newPosition;

%% simulating the flow related enhancement for different TRs
theta_deg = 18;
max_blood_delivery_time_ms = 1000;
TR_ms = [5, 10, 20, 30];

fig2 = figure; hold all;

for n = 1:numel(TR_ms)
    % number of RF pulses within that time
    mRF = 1:max_blood_delivery_time_ms/TR_ms(n);
    
    % equilibrium magnetization of the background tissue (grey matter)
    MzEgm = compute_equilibrium_magnetization(M0, TR_ms(n), T1_ms_gm, theta_deg);
    
    % longitudinal magnetization of moving blood water spins
    MzB = compute_longitudinal_magnetization(M0, TR_ms(n), T1_ms_blood, theta_deg, mRF');
    
    % relative FRE
    FRE = (MzB - MzEgm)./MzEgm;
    
    plot(mRF*TR_ms(n), FRE, 'LineWidth', 2);
end
yline(0, '--k', 'LineWidth', 2);
ylabel('relative flow-related enhancement'); xlabel('blood delivery time (ms)');
title('Flow-related enhancement as a function of repetition time TR');
legend({['TR = ', num2str(TR_ms(1), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(2), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(3), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(4), '%2.0f'), ' ms']});
ylim([-0.5 10]);
%% estimating the optimal flip angle for different BDTs
theta_deg = 1:90;
TR_ms = 20;
blood_delivery_time_ms = [100, 300, 500, 1000];
mRF = blood_delivery_time_ms/TR_ms;

% equilibrium magnetization of the background tissue (grey matter)
MzEgm = compute_equilibrium_magnetization(M0, TR_ms, T1_ms_gm, theta_deg);

% longitudinal magnetization of moving blood water spins
MzB = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_blood, theta_deg, mRF');
    
% relative FRE
FRE = (MzB - MzEgm)./MzEgm;

% plot
fig3 = figure;
plot(theta_deg, FRE, 'LineWidth', 2);
yline(0, '--k', 'LineWidth', 2);
ylabel('relative flow-related enhancement'); xlabel('excitation excitation flip angle \theta');
title('Flow-related enhancement as a function of excitation flip angle \theta');
legend({['blood delivery time = ', num2str(blood_delivery_time_ms(1), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(2), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(3), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(4), '%2.0f'), ' ms']});


%% Supplementary Material
% simulating the flow related enhancement for different TRs and different
% flip angles
theta_deg = 8;
max_blood_delivery_time_ms = 1000;
TR_ms = [5, 10, 20, 30];

fig4 = figure; hold all;

for n = 1:numel(TR_ms)
    
    % number of RF pulses within that time
    mRF = 1:max_blood_delivery_time_ms/TR_ms(n);
    
    % equilibrium magnetization of the background tissue (grey matter)
    MzEgm = compute_equilibrium_magnetization(M0, TR_ms(n), T1_ms_gm, theta_deg);
    
    % longitudinal magnetization of moving blood water spins
    MzB = compute_longitudinal_magnetization(M0, TR_ms(n), T1_ms_blood, theta_deg, mRF');
    
    % relative FRE
    FRE = (MzB - MzEgm)./MzEgm;
    
    plot(mRF*TR_ms(n), FRE, 'LineWidth', 2);
end
yline(0, '--k', 'LineWidth', 2);
ylabel('relative flow-related enhancement'); xlabel('blood delivery time (ms)');
title('Flow-related enhancement as a function of repetition time TR');
legend({['TR = ', num2str(TR_ms(1), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(2), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(3), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(4), '%2.0f'), ' ms']});
ylim([-0.5 10]);

% simulating the flow related enhancement for different TRs and different
% flip angles
theta_deg = 28;
max_blood_delivery_time_ms = 1000;
TR_ms = [5, 10, 20, 30];

fig5 = figure; hold all;

for n = 1:numel(TR_ms)
    
    % number of RF pulses within that time
    mRF = 1:max_blood_delivery_time_ms/TR_ms(n);
    
    % equilibrium magnetization of the background tissue (grey matter)
    MzEgm = compute_equilibrium_magnetization(M0, TR_ms(n), T1_ms_gm, theta_deg);
    
    % longitudinal magnetization of moving blood water spins
    MzB = compute_longitudinal_magnetization(M0, TR_ms(n), T1_ms_blood, theta_deg, mRF');
    
    % relative FRE
    FRE = (MzB - MzEgm)./MzEgm;
    
    plot(mRF*TR_ms(n), FRE, 'LineWidth', 2);
end
yline(0, '--k', 'LineWidth', 2);
ylabel('relative flow-related enhancement'); xlabel('blood delivery time (ms)');
title('Flow-related enhancement as a function of repetition time TR');
legend({['TR = ', num2str(TR_ms(1), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(2), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(3), '%2.0f'), ' ms'], ...
    ['TR = ', num2str(TR_ms(4), '%2.0f'), ' ms']});
ylim([-0.5 10]);
%% Supplementary Material
% estimating the optimal flip angle for different BDTs
theta_deg = 1:90;
TR_ms = 15;
blood_delivery_time_ms = [100, 300, 500, 1000];
mRF = blood_delivery_time_ms/TR_ms;

% equilibrium magnetization of the background tissue (grey matter)
MzEgm = compute_equilibrium_magnetization(M0, TR_ms, T1_ms_gm, theta_deg);

% longitudinal magnetization of moving blood water spins
MzB = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_blood, theta_deg, mRF');
    
% relative FRE
FRE = (MzB - MzEgm)./MzEgm;

% plot
fig6 = figure;
plot(theta_deg, FRE, 'LineWidth', 2);
yline(0, '--k', 'LineWidth', 2);
ylabel('relative flow-related enhancement'); xlabel('excitation excitation flip angle \theta');
title('Flow-related enhancement as a function of excitation flip angle \theta');
legend({['blood delivery time = ', num2str(blood_delivery_time_ms(1), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(2), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(3), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(4), '%2.0f'), ' ms']});

% estimating the optimal flip angle for different TRs
theta_deg = 1:90;
TR_ms = 25;
blood_delivery_time_ms = [100, 300, 500, 1000];
mRF = blood_delivery_time_ms/TR_ms;

% equilibrium magnetization of the background tissue (grey matter)
MzEgm = compute_equilibrium_magnetization(M0, TR_ms, T1_ms_gm, theta_deg);

% longitudinal magnetization of moving blood water spins
MzB = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_blood, theta_deg, mRF');
    
% relative FRE
FRE = (MzB - MzEgm)./MzEgm;

% plot
fig7 = figure;
plot(theta_deg, FRE, 'LineWidth', 2);
yline(0, '--k', 'LineWidth', 2);
ylabel('relative flow-related enhancement'); xlabel('excitation excitation flip angle \theta');
title('Flow-related enhancement as a function of excitation flip angle \theta');
legend({['blood delivery time = ', num2str(blood_delivery_time_ms(1), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(2), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(3), '%2.0f'), ' ms'], ...
    ['blood delivery time = ', num2str(blood_delivery_time_ms(4), '%2.0f'), ' ms']});