function vesselVolumeFraction = compute_vessel_volume_fraction(l_voxel,d_vessel)
%COMPUTE_VESSEL_VOLUME_FRACTION intersection between vessel and voxel
%
% compute radius
r_vessel = d_vessel/2;
theta = zeros(size(l_voxel));

% compute angle
% find position for the three cases
% case I) voxel completely fits into vessel
pos_pi_4 = l_voxel <= (d_vessel*cos(pi/4));
% case III) vessel fits completly into voxel
pos_zero = l_voxel >= d_vessel;
% case II) vessel and voxel intersect
pos_cos = (~pos_zero & ~pos_pi_4);

% assign theta
theta(pos_pi_4) = pi/4;
theta(pos_zero) = 0;
theta(pos_cos) = acos(l_voxel(pos_cos)/2/r_vessel);

% compute vessel volume
% case I)
V_inner(pos_pi_4) = l_voxel(pos_pi_4).^3;
% case III)
V_inner(pos_zero) = pi*r_vessel^2.*l_voxel(pos_zero);

% case II)
% compute area B
B = 1/2*r_vessel^2*(pi/4 - theta(pos_cos));

% compute length of third side
a = sin(theta(pos_cos)) * r_vessel;

% compute A
A = 1/2.*a.*l_voxel(pos_cos)/2;

V_inner(pos_cos) = 8.*(A+B).*l_voxel(pos_cos);


% compute voxel volume
V_voxel = l_voxel.^3;

% compute ratio
vesselVolumeFraction = V_inner./V_voxel;

end

