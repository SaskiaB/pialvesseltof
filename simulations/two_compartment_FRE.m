%% clean-up at start
clear; close all; clc;
%% parameters
M0 = 1; % thermal equilibrium magnetization before any RF pulse 
TR_ms = 20; % repetition time in ms
T1_ms_gm = 1950; % longitudinal relaxation time of grey matter from (ref)
T1_ms_blood = 2100; % longitudinal relaxation time of blood from (ref)
alphaErnst_gm = rad2deg(acos(exp(-TR_ms/T1_ms_gm))); % Ernst angle for grey matter
theta_deg = 18; % excitation flip angles
mRF = 1:50;
%% adding the multi-compartment model
MzEgm = compute_equilibrium_magnetization(M0, TR_ms, T1_ms_gm, theta_deg);
MzB = compute_longitudinal_magnetization(M0, TR_ms, T1_ms_blood, theta_deg, mRF);

% voxel size
voxelSize_um = 1:800;

% assuming a vessel with 200 um diamter
vesselDiameter_um = 200;

% compute the vessel volume fraction
vesselVolumeFraction =  compute_vessel_volume_fraction(voxelSize_um,vesselDiameter_um);
% computing the stationary tissue (equilibrium magnetization) comparment
tissueVolumeFraction = 1 - vesselVolumeFraction;

% computing the individual magnetization contributions
MTissueCompartment = tissueVolumeFraction' * MzEgm;
MBloodCompartment = vesselVolumeFraction' * MzB;

% hm: downward compatibility for pre-MATLAB2016b (no implicit expansion)
MTissueCompartment = repmat(MTissueCompartment, 1, size(mRF,2));

% computing the FRE with two compartments
FRE_MC = ((MBloodCompartment + MTissueCompartment) - MzEgm)./MzEgm;

% plot results
[X,Y] = meshgrid(voxelSize_um/1000, mRF*TR_ms);
fh = figure;
contourf(X,Y,FRE_MC', 15);
colormap(gray(15));
colorbar; fh.CurrentAxes.CLim = [0 4];
xlabel('Isotropic Voxel Size (mm) '); ylabel('Blood Delivery Time (ms)');
title('Relative Flow-Related Enhancement');
position = fh.Position;
newPosition = [position(1:2), 860, position(4)];
fh.Position = newPosition;