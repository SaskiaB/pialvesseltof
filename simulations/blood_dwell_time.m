clear; close all; clc;
%% Illustrate the blood dwell time
% (i.e. the time it takes the blood to traverse one voxel) as a function of
% blood velocity and voxel size
verbose = 0;

% define range of blood velocities and voxel sizes to simulate
blood_velocity_mm_s = 0.1:0.1:100;
voxel_size_mm = 0.01:0.01:0.8;

% create meshrgrid to simulate every possible combination
[S, V] = meshgrid(voxel_size_mm, blood_velocity_mm_s);
if verbose
    figure;
    subplot(1,2,1);
    imagesc(S);
    title('voxel size (mm)'); colorbar;
    subplot(1,2,2);
    imagesc(V);
    title('blood velocity (mm/s)'); colorbar;
end

% compute blood dwell time (v = s/t --> t = s/v)
blood_dwell_time_s = S./V;

% limit contour plot to 0.03 s (30 ms) for visualisation
blood_dwell_time_s(blood_dwell_time_s > 0.03) = 0.03;

% plot
fh = figure;
contourf(S, V, blood_dwell_time_s*1000, 25); hold all;
cmap = colormap('hot');
fh.CurrentAxes.Colormap = cmap(1:220, :);
colorbar;
xlabel('Isotropic Voxel Size (mm) '); ylabel('Blood Velocity (mm/s)');
title('Blood Dwell Time');

% add reference blood velocities

% Bouvy et al., 2016, NMR in Biomedicine
% Assessment of blood flow velocity and pulsatility in cerebral perforating
% arteries with 7-T quantitative flow MRI
Bouvy_velocity_mm_s = [5, 10, 39, 51];

% Kobari et al., 1984, Journal of Blood Flow and Metabolism
% Blood Flow Velocity in the Pial Arteries of Cats, with Particular 
% Reference to the Vessel Diameter
Kobari_velocity_mm_s = [12.9, 24.6, 42.1, 59.9];

% Nagaoka and Yoshida, 2006, Investigative Ophthalmology & Visual Science
% Noninvasive Evaluation of Wall Shear Stress on Retinal Microcirculation
% in Humans
Nagaoka_velocity_mm_s = [27.7, 41.1];

% add horizontal lines
for n = 1:numel(Bouvy_velocity_mm_s)
    p = yline(Bouvy_velocity_mm_s(n), 'Color', 'w', 'LineWidth', 2, 'Alpha', 1);
end

for n = 1:numel(Kobari_velocity_mm_s)
    p = yline(Kobari_velocity_mm_s(n), 'Color', 'w', 'LineWidth', 2, 'Alpha', 1, ...
        'LineStyle', '--');
end

for n = 1:numel(Nagaoka_velocity_mm_s)
    p = yline(Nagaoka_velocity_mm_s(n), 'Color', 'w', 'LineWidth', 2, 'Alpha', 1, ...
        'LineStyle', ':');
end
