function Mz = compute_longitudinal_magnetization(M0, TR_ms, T1_ms, theta_deg, mRF)
% computes the longitudinal magnetization Mz for a given M0, TR, T1, theta_deg,
% and number of RF pulses m based on equations 24.3 - 24.5 in
% Brown, R.W., Cheng, Y.-C.N., Haacke, E.M., Thompson, M.R., Venkatesan, R.,
% 2014. Chapter 24 - MR Angiography and Flow Quantification, in: Magnetic
% Resonance Imaging. John Wiley & Sons, Ltd, pp. 701�737.
% https://doi.org/10.1002/9781118633953.ch24

[MzE, ~, q] = compute_equilibrium_magnetization(M0, TR_ms, T1_ms, theta_deg);

% hm: downward compatibility for pre-MATLAB2016b (no implicit expansion)
mRF = repmat(mRF, 1, size(q,2));
q = repmat(q, size(mRF,1), 1);
MzE = repmat(MzE, size(mRF,1), 1);

Mz = MzE + q.^(mRF-1) .* (M0 - MzE);


end

