function [MzE, E1, q] = compute_equilibrium_magnetization(M0, TR_ms, T1_ms, theta_deg)
% computes the equilibrium magnetization MzE for a given M0, TR, T1, and theta_deg 
% based on equations 24.3 - 24.4 in 
% Brown, R.W., Cheng, Y.-C.N., Haacke, E.M., Thompson, M.R., Venkatesan, R.,
% 2014. Chapter 24 - MR Angiography and Flow Quantification, in: Magnetic 
% Resonance Imaging. John Wiley & Sons, Ltd, pp. 701�737.
% https://doi.org/10.1002/9781118633953.ch24

E1 = exp(-TR_ms/T1_ms);
q = E1.*cos(deg2rad(theta_deg));
MzE = M0 .* (1 - E1)./(1-q);


end