clear; clc; close all;
%% files
% hm: data in repository already masked; therefore, code adapted
rawPath = '/afm01/Q1/Q1709/MRA/data';
subjectID = 'twoEchoTOF';
rawDir = 'bias_correction';
%maskDir = 'masks';
segDir = 'vessel_segmentation_intensity';
segDir2 = 'vesselSegmentation';
%% load data
ID = 32;
% raw
rawName_e1 = dir(fullfile(rawPath, subjectID, rawDir, ['GRE*', num2str(ID), '_biasCor.nii']));
raw_e1 = MrImage(fullfile(rawName_e1.folder, rawName_e1.name));

rawName_e2 = dir(fullfile(rawPath, subjectID, rawDir, ['GRE*', num2str(ID), '_e2_biasCor.nii']));
raw_e2 = MrImage(fullfile(rawName_e2.folder, rawName_e2.name));

% mask
%maskName = dir(fullfile(rawPath, subjectID, maskDir, ['GRE*', num2str(ID), '_biasCor_mask.nii']));
%mask = MrImage(fullfile(maskName.folder, maskName.name));

% segmentation
segName_e1 = dir(fullfile(rawPath, subjectID, segDir, ['seg_GRE*', num2str(ID), '_biasCor*.nii']));
seg_e1 = MrImage(fullfile(segName_e1.folder, segName_e1.name));
seg_e1 = seg_e1.select('z', 1:207);

segName_e2 = dir(fullfile(rawPath, subjectID, segDir2, ['GRE*', num2str(ID), '_e2_biasCor_VR*.nii']));
seg_e2 = MrImage(fullfile(segName_e2.folder, segName_e2.name));
% combine
% hm: data in repository already masked; therefore, code adapted
m_e1 = raw_e1.select('z', 1:207);
m_e2 = raw_e2;
%m_e1 = raw_e1.select('z', 1:207).*mask;
%m_e2 = raw_e2.*mask;

% make skeleton
seg_e1_skel = seg_e1.copyobj();
seg_e1_skel.data = double(bwskel(logical(seg_e1.data)));
seg_e2_skel = seg_e2.copyobj();
seg_e2_skel.data = double(bwskel(logical(seg_e2.data)));
%% plot results
m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e2_skel.maxip('z'));

m_e1.select('z', 80:130).maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e2_skel.select('z', 80:130).maxip('z'), 'plotType', 'montage');

m_e1.select('x', 100:300).maxip('x').plot('rotate90', 2, ...
    'displayRange', [0 180], 'sliceDimension', 'x', ...
    'overlayImages', seg_e2_skel.select('x', 100:300).maxip('x'), ...
    'plotType', 'montage');

m_e1.maxip(3).plot('rotate90', 1, 'displayRange', [0 180]);
m_e2.maxip(3).plot('rotate90', 1, 'displayRange', [0 100]);

m_e1.maxip(2).remove_dims.plot('rotate90', 1, 'displayRange', [0 180]);
m_e2.maxip(2).remove_dims.plot('rotate90', 1, 'displayRange', [0 180]);

m_e1.maxip(1).remove_dims.plot('rotate90', 1, 'displayRange', [0 180]);
m_e2.maxip(1).remove_dims.plot('rotate90', 1, 'displayRange', [0 120]);

% binarize and threshold
v_e2 = m_e2.binarize(80);
v_e1 = m_e1.binarize(120);
mL_e1 = m_e1.copyobj();
mL_e1.data(mL_e1.data > 180) = 180;
mL_e2 = m_e2.copyobj();
mL_e2.data(mL_e2.data > 180) = 180;


mL_e1.maxip(3).plot('rotate90', 1, 'overlayImages', v_e2.maxip(3));
mL_e1.maxip(1).remove_dims.plot('rotate90', 1, ...
    'overlayImages', v_e2.maxip(1).remove_dims, 'overlayMode', 'edge');

mL_e2.maxip(2).remove_dims.plot('rotate90', 1, ...
    'overlayImages', v_e1.maxip(2).remove_dims);

mL_e2.maxip(1).remove_dims.plot('rotate90', 1, ...
    'overlayImages', v_e1.maxip(1).remove_dims);

v_e1.maxip(3).plot('rotate90', 1, 'overlayImages', v_e2.maxip(3));

seg_e1.maxip(3).plot('rotate90', 1, 'overlayImages', seg_e2.maxip(3));
seg_e1.maxip(1).remove_dims.plot('rotate90', 1, 'overlayImages', seg_e2.maxip(1).remove_dims);


segAND = seg_e1.copyobj();
segAND.data = zeros(seg_e1.dimInfo.nSamples);
segAND.data(seg_e1.data & seg_e2.data) = 1;

segXOR_e1 = seg_e1.copyobj();
segXOR_e1.data = zeros(seg_e1.dimInfo.nSamples);
segXOR_e1.data(xor(seg_e1.data, segAND.data)) = 1;

segXOR_e2 = seg_e2.copyobj();
segXOR_e2.data = zeros(seg_e2.dimInfo.nSamples);
segXOR_e2.data(xor(seg_e2.data, segAND.data)) = 1;

segAND.maxip(3).plot('rotate90', 1);
segXOR_e1.maxip(3).plot('rotate90', 1);
segXOR_e2.maxip(3).plot('rotate90', 1);

segXOR_e1.maxip(3).plot('rotate90', 1, 'overlayImages', segXOR_e2.maxip(3));

comb = seg_e1.copyobj();
comb.data = zeros(seg_e1.dimInfo.nSamples);
comb.data(segAND.data > 0) = 0;
comb.data(segXOR_e1.data > 0) = 1;
comb.data(segXOR_e2.data > 0) = -1;

diffSeg = seg_e1.copyobj();
diffSeg.data = zeros(seg_e1.dimInfo.nSamples);
diffSeg.data(segXOR_e1.data > 0) = 1;
diffSeg.data(segXOR_e2.data > 0) = -1;

%% export results
seg_e1.maxip(3).plot('rotate90', 1, ...
    'overlayImages', {seg_e2.select.maxip(3), ...
    segAND.maxip(3)}, 'plotType', 'montage');

seg_e1.maxip(1).remove_dims.plot('rotate90', 1, ...
    'overlayImages', {seg_e2.select.maxip(1).remove_dims, ...
    segAND.maxip(1).remove_dims}, 'plotType', 'montage');

diffSeg.sum(3).plot('rotate90', 1, 'displayRange', [-1 1], 'plotType', 'montage');

diffSeg.sum(1).remove_dims.plot('rotate90', 1, 'displayRange', [-1 1], 'plotType', 'montage');

seg_e1.select('z', 80:130).maxip(3).plot('rotate90', 1, ...
    'overlayImages', {seg_e2.select('z', 80:130).select.maxip(3), ...
    segAND.select('z', 80:130).maxip(3)}, 'plotType', 'montage');

seg_e1.select('x', 100:300).maxip(1).remove_dims.plot('rotate90', 1, ...
    'overlayImages', {seg_e2.select('x', 100:300).maxip(1).remove_dims, ...
    segAND.select('x', 100:300).maxip(1).remove_dims}, 'plotType', 'montage');

diffSeg.select('z', 80:130).sum(3).plot('rotate90', 1, 'displayRange', [-1 1], 'plotType', 'montage');

%% estimate displacement
seg_e2_mask = seg_e2.imdilate(strel('disk', 2));
seg_e1_skel_masked = seg_e1_skel.*seg_e2_mask;

m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e1.maxip('z'));
m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e2.maxip('z'));
m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e2_mask.maxip('z'));

m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e1_skel.maxip('z'));
m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e2_skel.maxip('z'));
m_e1.maxip('z').plot('rotate90', 1, 'displayRange', [0 180], ...
    'overlayImages', seg_e1_skel_masked.maxip('z'));

[ux,uy,uz] = HS3D(seg_e1_masked.data, seg_e2.data, 0.1, 100);
displ = m_e1.copyobj();
displ.data = sqrt(ux.^2 + uy.^2 + uz.^2);
displM = displ.*seg_e2_mask;
displM.name = 'voxel displacement';

displM.maxip('z').plot('rotate90', 1, ...
    'plotType', 'montage', 'colorMap', 'hot', 'displayRange', [0 4], ...
    'colorBar', 'on');

displM.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x', ...
    'plotType', 'montage', 'colorMap', 'hot', 'displayRange', [0 4], ...
    'colorBar', 'on');

displM.maxip('y').plot('rotate90', 1, 'sliceDimension', 'y', ...
    'plotType', 'montage', 'colorMap', 'hot', 'displayRange', [0 4], ...
    'colorBar', 'on');

bpData = displM.data(displM.data > 0);
figure; violinplot(bpData); ylim([0 4])