clear; clc; close all;
%% 0) Input filename and parameters
% hm: data in repository already masked; therefore, code adapted
dataPath = '/afm01/Q1/Q1709/MRA/data/twoEchoTOF';
segDir = 'vessel_segmentation_intensity';
biasCorDir = 'bias_correction';
%maskDir = 'masks';
artDir = 'arteries';
veinDir = 'veins';

segFiles = 'seg_GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_H75_L55_C10.nii';
biasCorFiles = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor.nii';
%maskFiles = 'GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_mask.nii';
artFiles = 'arteries_seg_GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_H75_L55_C10.nii';
veinFiles = 'veins_seg_GRE_3D_400um_TR20_FA18_TE7p5_14_sli52_FCY_GMP_BW200_32_biasCor_H75_L55_C10.nii';

%% 1) Load files
biasCorM = MrImage(fullfile(dataPath, biasCorDir, biasCorFiles));
seg = MrImage(fullfile(dataPath, segDir, segFiles));
arteries = MrImage(fullfile(dataPath, segDir, artDir, artFiles));
veins = MrImage(fullfile(dataPath, segDir, veinDir, veinFiles));

% last slice in T2Star (and, this, arteries and veins) might be missing due
% to recon error remove in seg as well to allow combination
if arteries.dimInfo.nSamples('z') ~= seg.dimInfo.nSamples('z')
    seg = seg.select('z', 1:seg.dimInfo.nSamples('z') - 1);
    seg.dimInfo.samplingPoints(3) = arteries.dimInfo.samplingPoints(3);
    seg.affineTransformation.update_from_affine_matrix(arteries.affineTransformation.affineMatrix);
end
% remove in biasCor as well to allow combination
if arteries.dimInfo.nSamples('z') ~= biasCorM.dimInfo.nSamples('z')
    biasCorM = biasCorM.select('z', 1:biasCorM.dimInfo.nSamples('z') - 1);
    biasCorM.dimInfo.samplingPoints(3) = arteries.dimInfo.samplingPoints(3);
    biasCorM.affineTransformation.update_from_affine_matrix(arteries.affineTransformation.affineMatrix);
end
% remove in mask as well to allow combination
% hm: data in repository already masked; therefore, code adapted
% if arteries.dimInfo.nSamples('z') ~= mask.dimInfo.nSamples('z')
%     mask = mask.select('z', 1:mask.dimInfo.nSamples('z') - 1);
%     mask.dimInfo.samplingPoints(3) = arteries.dimInfo.samplingPoints(3);
%     mask.affineTransformation.update_from_affine_matrix(arteries.affineTransformation.affineMatrix);
% end

seg.maxip('z').plot('rotate90', 1);

arteries.maxip('z').plot('rotate90', 1, 'overlayImages', ...
    {arteries.maxip('z'), veins.maxip('z')}, 'plotType', 'montage');
arteries.maxip('y').plot('rotate90', 1, 'overlayImages', ...
    {arteries.maxip('y'), veins.maxip('y')}, 'plotType', 'montage', ...
    'sliceDimension', 'y');
arteries.maxip('x').plot('rotate90', 2, 'overlayImages', ...
    {arteries.maxip('x'), veins.maxip('x')}, 'plotType', 'montage', ...
    'sliceDimension', 'x');
% make artery outline
arteriesOutline = arteries.imdilate(strel('disk', 1));
% plot overlays
biasCorM.maxip('z').plot('overlayImages', veins.maxip('z'), 'rotate90', 1, ...
    'overlayMode', 'edge', 'plotType', 'montage');
biasCorM.maxip('z').plot('overlayImages', arteries.maxip('z'), 'rotate90', 1, ...
    'overlayMode', 'edge', 'plotType', 'montage');
biasCorM.maxip('y').plot('overlayImages', veins.maxip('y'), 'rotate90', 1, ...
    'overlayMode', 'edge', 'sliceDimension', 'y', 'plotType', 'montage');
biasCorM.maxip('y').plot('overlayImages', ...
    arteriesOutline.maxip('y') - arteries.maxip('y'), 'rotate90', 1, ...
    'overlayMode', 'mask', 'sliceDimension', 'y', 'plotType', 'montage');
biasCorM.maxip('x').plot('overlayImages', veins.maxip('x'), 'rotate90', 2, ...
    'overlayMode', 'edge', 'sliceDimension', 'x', 'plotType', 'montage');
biasCorM.maxip('x').plot('overlayImages', ...
    arteriesOutline.maxip('x') -  arteries.maxip('x'), 'rotate90', 2, ...
    'overlayMode', 'mask', 'sliceDimension', 'x', 'plotType', 'montage');
% remove veins from data
biasCorMCP = biasCorM.copyobj();
biasCorMCP.data(logical(veins.data)) = 0;
biasCorMCP.maxip('z').plot('rotate90', 1, 'plotType', 'montage');
biasCorMCP.maxip('y').plot('rotate90', 1, 'sliceDimension', 'y', 'plotType', 'montage');
biasCorMCP.maxip('x').plot('rotate90', 2, 'sliceDimension', 'x', 'plotType', 'montage');

% save results
saveDir = 'cropForPlots';
biasCorM.parameters.save.path = fullfile(dataPath, saveDir);
seg.parameters.save.path = fullfile(dataPath, saveDir);
arteries.parameters.save.path = fullfile(dataPath, saveDir);
veins.parameters.save.path = fullfile(dataPath, saveDir);

biasCorM.parameters.save.fileName = 'biasCorM.nii';
seg.parameters.save.fileName = 'seg.nii';
arteries.parameters.save.fileName = 'arteries.nii';
veins.parameters.save.fileName = 'veins.nii';

biasCorM.save();
seg.save();
arteries.save();
veins.save();