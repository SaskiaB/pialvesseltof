clear; close all; clc;
%% 0) Input filename and parameters
% hm: data in repository already masked; therefore, code adapted
dataPath = '/QRISdata/Q1709/MRA/data_Magdeburg/Subject_02';
biasCorDir = 'biasCorrection';
%maskDir = 'masks';
segDir = 'vesselSegmentation';

fileID = 7;
filename = dir(fullfile(dataPath, biasCorDir, ['*_', num2str(fileID), '_biasCor.nii']));
%maskname = dir(fullfile(dataPath, maskDir, ['*_', num2str(fileID), '_biasCor_mask.nii']));
segname = dir(fullfile(dataPath, segDir, ['*_', num2str(fileID), '_biasCor*.nii']));

%% 1) Load data
biasCor = MrImage(fullfile(filename.folder, filename.name));
seg = MrImage(fullfile(segname(1).folder, segname(1).name));

overlayImage = seg.maxip('z').edge();
% apply mask; hm: data in repository already masked
maskedImage = biasCor;
%maskedImage = biasCor.*mask;

maskedImage.select('z', 50:60).maxip('z').plot('rotate90', 1, ...
    'x', 170:270, 'y', 950:1050, ...
    'overlayImages', seg.select('z', 50:60).maxip('z').edge());

maskedImage.select('z', 53:61).maxip('z').plot('rotate90', 1, ...
'x', 170:270, 'y', 950:1050, ...
'overlayImages', seg.select('z', 53:61).maxip('z').edge());

maskedImage.select('z', 45:61).maxip('z').plot('rotate90', 1, ...
'x', 170:270, 'y', 950:1050, ...
'overlayImages', seg.select('z', 45:61).maxip('z').edge());

idx = 1;
for z = 20:70
    fig = maskedImage.select('z', z).maxip('z').plot('rotate90', 1, ...
        'x', 170:270, 'y', 950:1050, ...
        'overlayImages', overlayImage);
    frame = getframe(fig);
    im{idx} = frame2im(frame);
    idx = idx + 1;
end

filename = '/winmounts/uqsboll2/uq-research/MRABOSTON-Q1709/MRA/data_Magdeburg/results/Subject_02/20203031_160um_and_140um_seg/singleVessel.gif';

for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.2);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.2);
    end
end
