clear; close all; clc;
%% 0) Input filename and parameters
% hm: data in repository already masked; therefore, code adapted
dataPath = '/QRISdata/Q1709/MRA/data_Magdeburg/Subject_02';
biasCorDir = 'biasCorrection';
%maskDir = 'masks';
segDir = 'vesselSegmentation';

fileID = 7;
filename = dir(fullfile(dataPath, biasCorDir, ['*_', num2str(fileID), '_biasCor.nii']));
%maskname = dir(fullfile(dataPath, maskDir, ['*_', num2str(fileID), '_biasCor_mask.nii']));
segname = dir(fullfile(dataPath, segDir, ['*_', num2str(fileID), '_biasCor*.nii']));

%% 1) Load data
biasCorM = MrImage(fullfile(filename.folder, filename.name));
seg = MrImage(fullfile(segname(1).folder, segname(1).name));

fig = figure;
%% animate for gif
for n = 1:seg.dimInfo.nSamples('z')-1
    minPos = max(1, n-2);
    maxPos = min(seg.dimInfo.nSamples('z'), n+2);
    pos = minPos:maxPos;
    data = biasCorM.select('z', pos).maxip('z').data;
    imagesc(rot90(data), [0 800]); colormap gray;
    daspect([1 1 1]); axis off;
    drawnow();
    frame = getframe(fig);
    im{n} = frame2im(frame);
end


resultsPath = '/QRISdata/Q1709/MRA/results/gifs';
filename = fullfile(resultsPath, 'highResMRA.gif');

for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.05);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
    end
end


%% plot segmentation
% version 1
samplingPoints = seg.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fv = isosurface(X, Y, Z, permute(seg.data, [2, 1, 3]), 0.5);
% plot
fig2 = figure;
p = patch(fv);
p.FaceColor = 'red';
p.EdgeColor = 'none';
camlight;
daspect([1 1 1]);
axis off;
view(0, 90);
zMin = min(samplingPoints{3});

for n = 1:numel(samplingPoints{3})-1
    zlim([zMin, samplingPoints{3}(n+1)]);
    drawnow();
    frame = getframe(fig2);
    im{n} = frame2im(frame);
end

filename = fullfile(resultsPath, 'highResMRASegmentation.gif');

for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.05);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
    end
end

%% version 2
% plot
fig3 = figure;
p = patch(fv);
p.FaceColor = 'red';
p.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
axis off;
view(0, 90);

zMax = [samplingPoints{3}(2:end), sort(samplingPoints{3}(2:end-1), 'descend')];
zMin = min(samplingPoints{3});

for n = 1:numel(zMax)
    zlim([zMin, zMax(n)]);
    drawnow();
    frame = getframe(fig3);
    im{n} = frame2im(frame);
end

filename = fullfile(resultsPath, 'highResMRASegmentationLongBlack.gif');

for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.05);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
    end
end


%% Subject_01 @ 160um
% hm: data in repository already masked
clear; close all; clc;
% input filename
dataPath = '/QRISdata/Q1709/MRA/data/Subject_01';
biasCorDir = 'bias_correction_corrected';
%maskDir = 'masks_corrected';
segDir = 'vessel_segmentation_intensity';
resultsPath = '/QRISdata/Q1709/MRA/results/gifs';
filenames = dir(fullfile(dataPath, biasCorDir, '*_biasCor_zipCor.nii'));
%filenamesMask = dir(fullfile(dataPath, maskDir, '*_biasCor_mask_c*.nii'));
filenamesSeg = dir(fullfile(dataPath, segDir, 'seg*.nii'));
% load individual slabs
for n = 1:numel(filenames)
    % bias cor image
    tmpBc{n} = MrImage(fullfile(dataPath, biasCorDir, filenames(n).name));
    tmpBc{n}.maxip('z').plot('rotate90',1);
    tmpBc{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    offset = tmpBc{1}.geometry.offcenter_mm(3) - tmpBc{n}.geometry.offcenter_mm(3);
    tmpBc{n}.dimInfo.samplingPoints{3} = tmpBc{n}.dimInfo.samplingPoints{3} - offset;
    
    % mask image; hm: data in repository already masked
    %tmpM{n} = MrImage(fullfile(dataPath, maskDir, filenamesMask(n).name));
    %tmpM{n}.maxip('z').plot('rotate90',1);
    %tmpM{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    %offset = tmpM{1}.geometry.offcenter_mm(3) - tmpM{n}.geometry.offcenter_mm(3);
    %tmpM{n}.dimInfo.samplingPoints{3} = tmpM{n}.dimInfo.samplingPoints{3} - offset;
    
    % segmentation
    tmpS{n} = MrImage(fullfile(dataPath, segDir, filenamesSeg(n).name));
    tmpS{n}.maxip('z').plot('rotate90',1);
    tmpS{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    offset = tmpS{1}.geometry.offcenter_mm(3) - tmpS{n}.geometry.offcenter_mm(3);
    tmpS{n}.dimInfo.samplingPoints{3} = tmpS{n}.dimInfo.samplingPoints{3} - offset;
    
    
end

% combine into one image
biasCorM = tmpBc{1}.combine(tmpBc, 'z', 1e-6);
%mask = tmpM{1}.combine(tmpM, 'z', 1e-6);
seg = tmpS{1}.combine(tmpS, 'z', 1e-6);
% plot results
biasCorM.maxip('z').plot('rotate90', 1);
%mask.maxip('z').plot('rotate90', 1);

%% compute voxel position
samplingPoints = seg.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(seg.data, [2, 1, 3]), 0.5);

% plot
fig4 = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;
view(0, 90);
fig4.CurrentAxes.CameraViewAngle = 10;

%% fourth version for twitter
% first, fly thorugh from top to bottom
view(0, 90);
CurrentZlim = zlim;
zLimMin = zeros(1, CurrentZlim(2));
zLimMax = 1:CurrentZlim(2);

% second, go from 0 90 to 0 35
b = 90:-1:35;
a = zeros(size(b));
% the go from 0 35 to 360 35
a = [a, 1:2:360];
b = [b, ones(1, 180) * 35];
% go back to intial position
d = 35:4:90;
c = zeros(size(d));

zLimMinTotal = [zLimMin, zeros(size(a)), zeros(size(c))];
zLimMaxTotal = [zLimMax, zLimMax(end) * ones(size(a)), zLimMax(end) * ones(size(c))];
aTotal = [zeros(size(zLimMin)), a, c];
bTotal = [b(1)*ones(size(zLimMin)), b, d];

fig4.CurrentAxes.CameraViewAngle = 10;
saveIdx = 1;
% animate for gif
for idx = 1:2:numel(zLimMinTotal)
    zlim([zLimMinTotal(idx), zLimMaxTotal(idx)]);
    view(aTotal(idx), bTotal(idx));
    drawnow();
    frame = getframe(fig4);
    im{saveIdx} = frame2im(frame);
    saveIdx = saveIdx+1;
end

filename = fullfile(resultsPath, 'highResMRASegmentation160.gif');
for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.05);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
    end
end

%% fifth for intro slide
samplingPoints = seg.dimInfo.samplingPoints;
zMax = [samplingPoints{3}(2:end), sort(samplingPoints{3}(2:end-1), 'descend')];
zMin = min(samplingPoints{3});

for n = 1:numel(zMax)
    zlim([zMin, zMax(n)]);
    drawnow();
    frame = getframe(fig4);
    im{n} = frame2im(frame);
end

filename = fullfile(resultsPath, 'highResMRASegmentation160umBlack.gif');

for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.05);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
    end
end

%% For MRA_P15 (supplementary figure)
resultsPath = '/afm01/Q1/Q1709/MRA/results/gifs';
resultsName = 'Supple_highResMRASegmentation160.gif';

nSamples = seg.dimInfo.nSamples;
[X, Y, Z] = meshgrid(0:nSamples(1)-1, 0:nSamples(2)-1, 0:nSamples(3)-1);
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(seg.data, [2, 1, 3]), 0.5);

% plot
fig6 = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;
view(45, 45);
fig6.CurrentAxes.CameraViewAngle = 10;
set(fig6, 'Color', 'k');

% first, fly thorugh from top to bottom
view(0, 90);
CurrentZlim = zlim;
zLimMin = zeros(1, CurrentZlim(2));
zLimMax = 1:CurrentZlim(2);

% second, go from 0 90 to 0 35
b = 90:-1:35;
a = zeros(size(b));
% the go from 0 35 to 360 35
a = [a, 1:2:360];
b = [b, ones(1, 180) * 35];
% go back to intial position
d = 35:4:90;
c = zeros(size(d));

zLimMinTotal = [zLimMin, zeros(size(a)), zeros(size(c))];
zLimMaxTotal = [zLimMax, zLimMax(end) * ones(size(a)), zLimMax(end) * ones(size(c))];
aTotal = [zeros(size(zLimMin)), a, c];
bTotal = [b(1)*ones(size(zLimMin)), b, d];

fig6.CurrentAxes.CameraViewAngle = 10;
saveIdx = 1;
% animate for gif
for idx = 1:2:numel(zLimMinTotal)
    zlim([zLimMinTotal(idx), zLimMaxTotal(idx)]);
    view(aTotal(idx), bTotal(idx));
    drawnow();
    frame = getframe(fig6);
    im{saveIdx} = frame2im(frame);
    saveIdx = saveIdx+1;
end

filename = fullfile(resultsPath, resultsName);
for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 0.05);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
    end
end