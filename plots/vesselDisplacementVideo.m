clear; clc; close all;
%% files
% hm: data in repository already masked; therefore, code adapted
rawPath = '/QRISdata/Q1709/MRA/data';
subjectID = 'twoEchoTOF';
rawDir = 'bias_correction';
%maskDir = 'masks';

ID = 32;
% raw
rawName_e1 = dir(fullfile(rawPath, subjectID, rawDir, ['GRE*', num2str(ID), '_biasCor.nii']));
raw_e1 = MrImage(fullfile(rawName_e1.folder, rawName_e1.name));

rawName_e2 = dir(fullfile(rawPath, subjectID, rawDir, ['GRE*', num2str(ID), '_e2_biasCor.nii']));
raw_e2 = MrImage(fullfile(rawName_e2.folder, rawName_e2.name));

% mask
%maskName = dir(fullfile(rawPath, subjectID, maskDir, ['GRE*', num2str(ID), '_biasCor_mask.nii']));
%mask = MrImage(fullfile(maskName.folder, maskName.name));

% apply mask
% hm: data in repository already masked; therefore, code adapted
m_e1 = raw_e1.select('z', 1:207);
m_e2 = raw_e2;
%m_e1 = raw_e1.select('z', 1:207).*mask.select('z', 1:207);
%m_e2 = raw_e2.*mask.select('z', 1:207);

%% plot raw data
m_e1.maxip('z').plot('rotate90', 1);
m_e2.maxip('z').plot('rotate90', 1);

maxipDirection = 'sagittal';

switch maxipDirection
    case 'axial'
        % axial
        I1 = m_e1.maxip('z').data;
        I2 = m_e2.maxip('z').data;
    case 'sagittal'
        % sagittal
        I1 = squeeze(m_e1.select('x', 100:300).maxip('x').data);
        I2 = squeeze(m_e2.select('x', 100:300).maxip('x').data);
end

fig = figure;
% animate for gif
% echo 1
imagesc(rot90(I1), [0 250]); colormap gray;
daspect([1 1 1]); axis off;
drawnow();
frame = getframe(fig);
im{1} = frame2im(frame);

% echo 2
imagesc(rot90(I2), [0 250]); colormap gray;
daspect([1 1 1]); axis off;
drawnow();
frame = getframe(fig);
im{2} = frame2im(frame);

resultsPath = '/QRISdata/Q1709/MRA/results/gifs';
filename = fullfile(resultsPath, ...
    ['vesselDisplacement_', maxipDirection, '.gif']);

for idx = 1:numel(im)
    [A, map] = rgb2ind(im{idx}, 256);
    if idx == 1
        imwrite(A, map, filename, 'gif', 'loopCount', Inf, 'DelayTime', 1);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 1);
    end
end