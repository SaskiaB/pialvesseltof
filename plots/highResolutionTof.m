%% Subject_01 @ 160um
% hm: data in repository already masked; therefore, code adapted
clear; close all; clc;
% input filename
dataPath = '/afm01/Q1/Q1709/MRA/data/Subject_01';
biasCorDir = 'bias_correction_corrected';
%maskDir = 'masks_corrected';
segDir = 'vesselSegInt_denIm';
filenames = dir(fullfile(dataPath, biasCorDir, '*_biasCor_zipCor.nii'));
%filenamesMask = dir(fullfile(dataPath, maskDir, '*_biasCor_mask_c*.nii'));
filenamesSeg = dir(fullfile(dataPath, segDir, 'seg*.nii'));
% load individual slabs
for n = 1:numel(filenames)
    % bias cor image
    tmpBc{n} = MrImage(fullfile(dataPath, biasCorDir, filenames(n).name));
    tmpBc{n}.maxip('z').plot('rotate90',1);
    tmpBc{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    offset = tmpBc{1}.geometry.offcenter_mm(3) - tmpBc{n}.geometry.offcenter_mm(3);
    tmpBc{n}.dimInfo.samplingPoints{3} = tmpBc{n}.dimInfo.samplingPoints{3} - offset;
    
    % mask image hm: data in repository already masked
    %tmpM{n} = MrImage(fullfile(dataPath, maskDir, filenamesMask(n).name));
    %tmpM{n}.maxip('z').plot('rotate90',1);
    %tmpM{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    %offset = tmpM{1}.geometry.offcenter_mm(3) - tmpM{n}.geometry.offcenter_mm(3);
    %tmpM{n}.dimInfo.samplingPoints{3} = tmpM{n}.dimInfo.samplingPoints{3} - offset;
    
    % segmentation
    tmpS{n} = MrImage(fullfile(dataPath, segDir, filenamesSeg(n).name));
    tmpS{n}.maxip('z').plot('rotate90',1);
    tmpS{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    offset = tmpS{1}.geometry.offcenter_mm(3) - tmpS{n}.geometry.offcenter_mm(3);
    tmpS{n}.dimInfo.samplingPoints{3} = tmpS{n}.dimInfo.samplingPoints{3} - offset;
    
    
end

% combine into one image
biasCorM = tmpBc{1}.combine(tmpBc, 'z', 1e-6);
%mask = tmpM{1}.combine(tmpM, 'z', 1e-6);
seg = tmpS{1}.combine(tmpS, 'z', 1e-6);
% plot results
biasCorM.maxip('z').plot('rotate90', 1);
%mask.maxip('z').plot('rotate90', 1);

% plot

% plot

biasCorM.maxip('z').plot('rotate90', 1, 'overlayImages', seg.maxip('z'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [300 600]);

biasCorM.select('y', 400:950).maxip('y').plot('sliceDimension', 'y', ...
    'rotate90', 1, 'overlayImages', seg.select('y', 400:950).maxip('y'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [300 600]);

biasCorM.maxip('x').flip('z').plot(...
    'displayRange', [350 600], 'plotType', 'montage', ...
    'overlayImages', seg.maxip('x').flip('z').edge('sobel', 0),...
    'overlayMode', 'mask', 'sliceDimension', 'x');

biasCorM.select('y', 400:950).maxip('x').flip('z').plot(...
    'displayRange', [200 550], 'plotType', 'montage', ...
    'overlayImages', seg.select('y', 400:950).maxip('x').flip('z').edge('sobel', 0),...
    'overlayMode', 'mask', 'sliceDimension', 'x');

biasCorM.maxip('y').plot('sliceDimension', 'y', 'rotate90', 1);


biasCorM.select('x', 1:550).maxip('x').flip('z').plot(...
    'displayRange', [200 550], 'plotType', 'montage', ...
    'overlayImages', seg.select('x', 1:550).maxip('x').flip('z').edge('sobel', 0),...
    'overlayMode', 'mask', 'sliceDimension', 'x');

% plot segmentation only in 3D
samplingPoints = seg.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(seg.data, [2, 1, 3]), 0.5);

% plot
fig = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;
view(45, 40);
fig.CurrentAxes.CameraViewAngle = 10;

% additional plots with empty segmentation overlay for the supplementary
% material
emptySeg = seg.copyobj();
emptySeg.data = zeros(emptySeg.dimInfo.nSamples);
biasCorM.maxip('z').plot('rotate90', 1, 'overlayImages', emptySeg.maxip('z'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [300 600]);

biasCorM.select('y', 400:950).maxip('y').plot('sliceDimension', 'y', ...
    'rotate90', 1, 'overlayImages', emptySeg.select('y', 400:950).maxip('y'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [300 600]);

biasCorM.maxip('x').flip('z').plot(...
    'displayRange', [350 600], 'plotType', 'montage', ...
    'overlayImages', emptySeg.maxip('x').flip('z').edge('sobel', 0),...
    'overlayMode', 'mask', 'sliceDimension', 'x');
%% Subject_02 @ 140um
% hm: data in repository already masked; therefore, code adapted
clear; close all; clc;
% input filename
dataPath = '/clusterdata/uqsboll2/data/MRABOSTON-Q1709/MRA/data_Magdeburg/Subject_02';
biasCorDir = 'biasCorrection/arteries';
%maskDir = 'masks';
segDir = 'vesselSegInt_denIm/arteries/';

filename = dir(fullfile(dataPath, biasCorDir, '*140um*.nii'));
%filenameMask = dir(fullfile(dataPath, maskDir, '*140um*_mask.nii'));
filenameSeg = dir(fullfile(dataPath, segDir, '*140um*.nii'));
% load data
biasCorM = MrImage(fullfile(dataPath, biasCorDir, filename.name));
seg = MrImage(fullfile(dataPath, segDir, filenameSeg(1).name));

% plot data
biasCorM.maxip('z').plot('rotate90', 1, 'overlayImages', seg.maxip('z'), ...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 1100]);

biasCorM.maxip('x').plot('sliceDimension', 'x', 'rotate90', 2, ...
    'overlayImages', seg.maxip('x'), 'overlayMode', 'edge', ...
    'plotType', 'montage', 'displayRange', [200 1100]);

biasCorM.maxip('y').plot('rotate90', 1, 'sliceDimension', 'y', ...
    'overlayImages', seg.maxip('y'), 'overlayMode', 'edge', ...
    'plotType', 'montage', 'displayRange', [200 1100]);

% additional plots with empty segmentation overlay for the supplementary
% material
emptySeg = seg.copyobj();
emptySeg.data = zeros(emptySeg.dimInfo.nSamples);

biasCorM.maxip('z').plot('rotate90', 1, 'overlayImages', emptySeg.maxip('z'), ...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 1100]);

biasCorM.maxip('x').plot('sliceDimension', 'x', 'rotate90', 2, ...
    'overlayImages', emptySeg.maxip('x'), 'overlayMode', 'edge', ...
    'plotType', 'montage', 'displayRange', [200 1100]);

biasCorM.maxip('y').plot('rotate90', 1, 'sliceDimension', 'y', ...
    'overlayImages', emptySeg.maxip('y'), 'overlayMode', 'edge', ...
    'plotType', 'montage', 'displayRange', [200 1100]);

% biasCorM.select('y', 300:1100).maxip('y').plot('rotate90', 1, 'sliceDimension', 'y', ...
%     'overlayImages', seg.select('y', 300:1100).maxip('y'), 'overlayMode', 'edge', ...
%     'plotType', 'montage', 'displayRange', [200 1100]);

% biasCorM.maxip('y').plot('sliceDimension', 'y', 'rotate90', 1);
% 
% biasCorM.maxip('x').plot('sliceDimension', 'x', 'rotate90', 2);

%% plot segmentation only in 3D
samplingPoints = seg.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(seg.data, [2, 1, 3]), 0.5);

% plot
fig = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;
view(35, 60);
fig.CurrentAxes.CameraViewAngle = 10;

%% plot potential
biasCorM.select('z', 20:80).maxip('z').plot('rotate90', 1, 'overlayImages', ...
    seg.select('z', 20:80).maxip('z'), ...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 800], ...
    'x', 175:270, 'y', 960:1030);

biasCorM.select('z', 80:120).maxip('z').plot('rotate90', 1, 'overlayImages', ...
    seg.select('z', 80:120).maxip('z'), ...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 1000], ...
    'x', 650:900, 'y', 1000:1100);

for z = 80:120
    biasCorM.select('z', z).plot('rotate90', 1, 'overlayImages', ...
    seg.select('z', 80:120).maxip('z'), ...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 1000], ...
    'x', 650:900, 'y', 1000:1100);
end
% smallBiasCor = biasCorM.select('z', 20:80, 'y', 960:1030, 'x', 175:270);
% smallSeg = seg.select('z', 20:80, 'y', 960:1030, 'x', 175:270);
% smallBiasCor.parameters.save.path = fullfile(dataPath, 'potential');
% smallBiasCor.parameters.save.fileName = 'biasCor_ex1_z_20_80_y_960_1030_x_175_270.nii';
% smallSeg.parameters.save.path = fullfile(dataPath, 'potential');
% smallSeg.parameters.save.fileName = 'seg_ex1_z_20_80_y_960_1030_x_175_270.nii';
% smallBiasCor.save();
% smallSeg.save();

% load manual segmentation
m = MrImage(fullfile(dataPath, 'potential', 'seg_ex1_z_20_80_y_960_1030_x_175_270_mV2.nii'));

biasCorM.select('z', 20:80, 'x', 175:270, 'y', 960:1030).maxip('z').plot(...
    'rotate90', 1, 'overlayImages', m.maxip('z'), ...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 800]);

samplingPoints = m.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(m.data, [2, 1, 3]), 0.5);

% plot
fig = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;