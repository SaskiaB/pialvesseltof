%% Subject_03 @ 160um
% hm: data in repository already masked; therefore, code adapted
clear; close all; clc;
% input filename
% dataPath = '/winmounts/uqsboll2/data.cai.uq.edu.au/MRABOSTON-Q1709/MRA/data/Subject_03';
dataPath = '/afm01/Q1/Q1709/MRA/data/Subject_03';
biasCorDir = 'bias_correction';
%maskDir = 'masks';
segDir = 'vesselSegInt_denIm';
filenames = dir(fullfile(dataPath, biasCorDir, '*_biasCor.nii'));
%filenamesMask = dir(fullfile(dataPath, maskDir, '*_biasCor_mask.nii'));
filenamesSeg = dir(fullfile(dataPath, segDir, '*_biasCor_noiseCor_VT*.nii'));
% load individual slabs
for n = 1:numel(filenames)
    % bias cor image
    tmpBc{n} = MrImage(fullfile(dataPath, biasCorDir, filenames(n).name));
    tmpBc{n}.maxip('z').plot('rotate90',1);
    tmpBc{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    offset = tmpBc{1}.geometry.offcenter_mm(3) - tmpBc{n}.geometry.offcenter_mm(3);
    tmpBc{n}.dimInfo.samplingPoints{3} = tmpBc{n}.dimInfo.samplingPoints{3} - offset;
    
    % mask image; hm: data already masked
    %tmpM{n} = MrImage(fullfile(dataPath, maskDir, filenamesMask(n).name));
    %tmpM{n}.maxip('z').plot('rotate90',1);
    %tmpM{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    %offset = tmpM{1}.geometry.offcenter_mm(3) - tmpM{n}.geometry.offcenter_mm(3);
    %tmpM{n}.dimInfo.samplingPoints{3} = tmpM{n}.dimInfo.samplingPoints{3} - offset;
    
    % segmentation
    tmpS{n} = MrImage(fullfile(dataPath, segDir, filenamesSeg(n).name));
    tmpS{n}.maxip('z').plot('rotate90',1);
    tmpS{n}.maxip('x').flip('z').plot('sliceDimension', 'x');
    offset = tmpS{1}.geometry.offcenter_mm(3) - tmpS{n}.geometry.offcenter_mm(3);
    tmpS{n}.dimInfo.samplingPoints{3} = tmpS{n}.dimInfo.samplingPoints{3} - offset;
    
    
end

% combine into one image
biasCorM = tmpBc{1}.combine(tmpBc, 'z', 1e-4);
%mask = tmpM{1}.combine(tmpM, 'z', 1e-4);
seg = tmpS{1}.combine(tmpS, 'z', 1e-4);
% plot results
biasCorM.maxip('z').plot('rotate90', 1);
%mask.maxip('z').plot('rotate90', 1);


biasCorM.maxip('y').plot('sliceDimension', 'y', 'rotate90', 1);


biasCorM.maxip('z').plot('rotate90', 1, 'overlayImages', seg.maxip('z'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 700], ...
    'x', 90:1010, 'y', 160:1190);

biasCorM.select('y', 400:950).maxip('y').plot('sliceDimension', 'y', ...
    'rotate90', 1, 'overlayImages', seg.select('y', 400:950).maxip('y'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 750], ...
    'x', 90:1010);

biasCorM.maxip('x').flip('z').plot(...
    'displayRange', [200 750], 'plotType', 'montage', ...
    'overlayImages', seg.maxip('x').flip('z').edge('sobel', 0),...
    'overlayMode', 'mask', 'sliceDimension', 'x', 'y', 160:1190);

% plot segmentation only in 3D
samplingPoints = seg.dimInfo.samplingPoints({'x', 'y', 'z'});
[X, Y, Z] = meshgrid(samplingPoints{1}, samplingPoints{2}, samplingPoints{3});
% create isosurface
% matlab switches x and y, so we have to do the same
fvTOF = isosurface(X, Y, Z, permute(seg.data, [2, 1, 3]), 0.5);

% plot
fig = figure;
t = patch(fvTOF);
t.FaceColor = [0.9 0 0];
t.EdgeColor = 'none';
lighting none;
daspect([1 1 1]);
camlight;
axis off;
view(45, 45);
fig.CurrentAxes.CameraViewAngle = 10;

% additional plots with empty segmentation overlay for the supplementary
% material
emptySeg = seg.copyobj();
emptySeg.data = zeros(emptySeg.dimInfo.nSamples);

biasCorM.maxip('z').plot('rotate90', 1, 'overlayImages', emptySeg.maxip('z'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 700], ...
    'x', 90:1010, 'y', 160:1190);

biasCorM.select('y', 400:950).maxip('y').plot('sliceDimension', 'y', ...
    'rotate90', 1, 'overlayImages', emptySeg.select('y', 400:950).maxip('y'),...
    'overlayMode', 'edge', 'plotType', 'montage', 'displayRange', [200 750], ...
    'x', 90:1010);

biasCorM.maxip('x').flip('z').plot(...
    'displayRange', [200 750], 'plotType', 'montage', ...
    'overlayImages', emptySeg.maxip('x').flip('z').edge('sobel', 0),...
    'overlayMode', 'mask', 'sliceDimension', 'x', 'y', 160:1190);