clear; close all; clc
%% Load data
% hm: data in repository already masked; therefore, code adapted
dataPath = '/afm01/Q1/Q1709/MRA/data_Magdeburg/Subject_02';
biasCorDir = 'biasCorrection';
%maskDir = 'masks';

filename_140 = 'TOF_hm_xpace_140um_MoCoOn_20200220145234_7_biasCor.nii';
filename_160 = 'TOF_hm_xpace_160_MoCoOn_20200220145234_19_biasCor_alignedto7.nii';
%filename_mask = 'TOF_hm_xpace_160_MoCoOn_20200220145234_19_biasCor_maskalignedTo7.nii';

mI140 = MrImage(fullfile(dataPath, biasCorDir, filename_140));
mI160 = MrImage(fullfile(dataPath, biasCorDir, filename_160));

%% Plot data
mI140.maxip('z').plot('rotate90', 1);
mI160.maxip('z').plot('rotate90', 1);

mI140.maxip('z').plot('rotate90', 1, 'x', 200:300, 'y', 700:850, ...
    'displayRange', [350 1000], 'plotType', 'montage');
mI160.maxip('z').plot('rotate90', 1, 'x', 200:300, 'y', 700:850, ...
    'displayRange', [350 1000], 'plotType', 'montage');

mI140.maxip('z').plot('rotate90', 1, 'x', 700:800, 'y', 950:1100, ...
    'displayRange', [350 1000], 'plotType', 'montage');
mI160.maxip('z').plot('rotate90', 1, 'x', 700:800, 'y', 950:1100, ...
    'displayRange', [350 1000], 'plotType', 'montage');

mI140.maxip('z').plot('rotate90', 1, 'x', 150:250, 'y',850:1000, ...
    'displayRange', [350 1000], 'plotType', 'montage');
mI160.maxip('z').plot('rotate90', 1, 'x', 150:250, 'y',850:1000, ...
    'displayRange', [350 1000], 'plotType', 'montage');

mI140.maxip('z').plot('rotate90', 1, 'x', 475:575, 'y', 850:1000, ...
    'displayRange', [350 1000], 'plotType', 'montage');
mI160.maxip('z').plot('rotate90', 1, 'x', 475:575, 'y', 850:1000, ...
    'displayRange', [350 1000], 'plotType', 'montage');

mI140.maxip('z').plot('rotate90', 1, 'x', 200:300, 'plotType', 'montage');
